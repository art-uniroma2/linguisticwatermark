 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is DictInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * DictInterface was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about DictInterface can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Donato Griesi
  */

/*
 * Created on Mar 30, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package it.uniroma2.art.lw.dict;

import java.util.Vector;

/**
 * @author Donato Griesi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConceptualizedElement {			
		private Vector<DictConcept> concepts;
		private Translations translations;
		
		public ConceptualizedElement() {
			// insieme delle concettualizzazioni
			concepts = new Vector<DictConcept>();			
			// insieme delle traduzioni relative alle concettualizzazioni
			translations = new Translations();
		}
		
		public void setConceptualizedTranslation(String lemma, int elementPosition, String[] translationsForConcept) {													
			DictConcept dictConcept = new DictConcept(lemma, elementPosition, concepts.size());
			concepts.add(dictConcept);
			translations.add(translationsForConcept);
		}			
		
		public String[] getTranslations(DictConcept dictConcept) {								
			String[] translationsForConcept = (String[])translations.get(dictConcept.getRelativePositionConcept());						
			return translationsForConcept;
		}
		
		public DictConcept[] getConcepts() {
			Vector<DictConcept> vector = new Vector<DictConcept>();
			for (int i = 0; i < concepts.size(); i++) {
				DictConcept dictConcept = concepts.get(i);
				vector.add(dictConcept);
			}
			DictConcept[] dictConceptArray = new DictConcept[vector.size()];
			for (int i = 0; i < vector.size(); i++) {
				dictConceptArray[i] = vector.get(i);
			}
			return dictConceptArray;			
		}
			
		public DictConcept getConceptualizationByID(String lemma, int relativePositionConcept) {						
			DictConcept dictConcept; 			 						
			for (int i = 0; i < concepts.size(); i++) {
				dictConcept = concepts.get(i);												
				if (dictConcept.getLemma().equals(lemma)) {				
					if (dictConcept.getRelativePositionConcept() == relativePositionConcept) {						
						return dictConcept;
					}
				}
			}			
			return null;
		}		
}
