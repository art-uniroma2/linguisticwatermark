 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is DictInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * DictInterface was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about DictInterface can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Donato Griesi
  */
 
/*
 * Created on Mar 29, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package it.uniroma2.art.lw.dict;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;

import java.awt.Frame;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import ar.com.ktulu.dict.Database;
import ar.com.ktulu.dict.DatabaseException;
import ar.com.ktulu.dict.DatabaseFactory;
import ar.com.ktulu.dict.Matches;
import ar.com.ktulu.dict.VirtualDatabase;
import ar.com.ktulu.dict.strategies.Strategy;

/**
 * @author Donato Griesi
 * @author Armando Stellato
 *
 */
public class Dict {

	private String language = null;
	private Database database = null;
	private Strategy strategy = null;
	private Vector<ConceptualizedElement> conceptualizedSet = null;
	private String previousLemma = "";	
	 	
	/** Dictionaries. */
	private LinkedList<String> dbs;
	private Hashtable<String, Database> hash_dbs;
	private Hashtable<String, Strategy> strategies;
	
	
	private void init(String[] args) throws IOException, ClassNotFoundException,
	IllegalAccessException, InstantiationException {
		Properties props = processCL(args);
		if (props == null)
		    showErrorMsg("some problems occurred during dict initialization\ncheck property file: dict.properties.\n");
		readConfig(props.getProperty("conf"));
		initStrategies(props.getProperty("strats"));
	}

	static void showErrorMsg(String errorMsg) {
		/*TODO I should change initialize() in LWatermark to throw exceptions, which may be caught by the application using the Linguistic Watermark package*/
		//          custom title, error icon
		Frame frame = JOptionPane.getRootFrame();
		JOptionPane.showMessageDialog(frame, errorMsg, "Dict Interface Error!", JOptionPane.ERROR_MESSAGE);
		System.out.println(errorMsg);	    
	}
	
	private boolean isLogging() {
		//fixme: enable/disable it as the user whishes to
		return true;
	}

	private void loginfo(String s) {
		//fixme: here logging should be directed to whatever the user
		//selected
		System.err.print(s);
	}

	/** Returns the default base dir for the configuration files. */
        private String getConfBaseDir() {
            return "./";
        }

	/**
	 * Returns a new string with minimum length l. If the length of s is
	 * shorter than l, spaces are added at the end of s.
	 */
	private String ensureLength(String s, int l) {
		if (s.length() > l) return s;
		else {
			StringBuffer b = new StringBuffer(s);
			while (b.length() != l) b.append(" ");
			return b.toString();
		}
	}

	/** Parses command line arguments */
	private Properties processCL(String[] args) {
		if (args == null) return null;
		Properties props = new Properties(getDefaultConfig());
		// This is the command line argument to properties names
		Hashtable<String, String> cl2conf = new Hashtable<String, String>();

		//same config args as dictd
		cl2conf.put("-c", "conf");
		cl2conf.put("--config", "conf");		
		cl2conf.put("--strats", "strats");

		int i = 0;
		while (i < args.length) {
			String cn = cl2conf.get(args[i]);
			if (cn != null && i < args.length-1)
				props.setProperty(cn, args[++i]);
			i++;
		}
		return props;
	}

	private boolean propIsNumber(Properties props, String name) {
		try {
			Integer.valueOf(props.getProperty(name)).intValue();
			return true;
		} catch (NumberFormatException e) {
			System.err.println("The property " + name +
				" is not a number.");
			return false;
		}
	}

	private Properties getDefaultConfig() {
		Properties p = new Properties();
		
		p.setProperty("conf", getConfBaseDir() + "databases.xml");
		p.setProperty("strats", getConfBaseDir() + "strategies.xml");
		return p;
	}

	private void initStrategies(String conf) throws IOException,
	ClassNotFoundException, IllegalAccessException, InstantiationException
	{
		Document document = null;
		try {
			document = readDOMDocument(conf);
		}
		catch (SAXException e) {
			//fixme: this sucks
			throw new IOException("initStrategies: "+e.getMessage());
		}
		catch (ParserConfigurationException e) {
			//fixme: this sucks
			throw new IOException("initStrategies: "+e.getMessage());
		}

		
		Element elem = document.getDocumentElement();
		NodeList dicts = elem.getElementsByTagName("strategy");
		strategies = new Hashtable<String, Strategy>();
		String s;
		for (int i = 0; i < dicts.getLength(); i++) {
			Node node = dicts.item(i);

			if (!node.hasChildNodes())
				throw new IOException("Error: empty strategy");
			Strategy strat = (Strategy)Class.forName(node.getFirstChild().getNodeValue()).newInstance();
			strategies.put(strat.getName(), strat);
		}
		if (strategies.size() == 0)
			throw new IOException("Warning: no strategies were loaded!");
		else {
			if (strategies.get("exact") == null)
				System.err.println("Warning: exact strategy was not loaded!");
			if (strategies.get("prefix") == null)
				System.err.println("Warning: prefix strategy was not loaded!");
		}
	}

	private Document readDOMDocument(String fname)
		throws IOException, SAXException, ParserConfigurationException
	{
		DocumentBuilderFactory factory;

		factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		factory.setIgnoringComments(true);

		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setErrorHandler(
			new ErrorHandler() {
				public void error(SAXParseException exception)
				throws SAXParseException {
					throw exception;
				}
				public void fatalError(SAXParseException exception)
				throws SAXParseException {
					throw exception;
				}
				public void warning(SAXParseException exception)
				throws SAXParseException {
					throw exception;
				}
			});
		Document document = builder.parse(fname);
		return document;
	}

	/** Parses dictd.conf and initializes the databases defined in it. */
	private void readConfig(String conf) throws IOException
	{		
		Document document = null;
		try {
			document = readDOMDocument(conf);
		} catch (SAXException e) {
			//fixme: this sucks
			throw new IOException(e.getMessage());
		} catch (ParserConfigurationException e) {
			//fixme: this sucks
			throw new IOException(e.getMessage());
		}

		dbs = new LinkedList<String>();
		hash_dbs = new Hashtable<String, Database>();

		Element elem = document.getDocumentElement();
		NodeList dicts = elem.getElementsByTagName("dictionary");
		String basedir = elem.getAttribute("base");
		for (int i = 0; i < dicts.getLength(); i++) {
			Node node = dicts.item(i);

			NamedNodeMap attrs = node.getAttributes();
                        String name = attrs.getNamedItem("name").getNodeValue();                        
                      if (!name.equals(language)) 
                      	continue;
                        try {
                                HashMap<String, String> atrs = new HashMap<String, String>();
                                for (int j=0; j < attrs.getLength(); j++) {
                                    Node n = attrs.item(j);                                    
                                    atrs.put(n.getNodeName(), n.getNodeValue());
                                }
                                Database db = DatabaseFactory.createDatabase(basedir, atrs);
                hash_dbs.put(name, db);
				dbs.add(name);

				loginfo(":I: " + ensureLength(db.getName(), 12)
					+ " " + db.keySize() + " headwords\n");
			} catch (DatabaseException e) {
				System.err.println(e);
			}
		}
	}

	private Database getDatabase(String name) {
		if (name.equals("!") || name.equals("*"))
			return new VirtualDatabase(name, hash_dbs.values());
		return hash_dbs.get(name);
	}

	private int databaseSize() {
		return dbs.size();
	}

	private Iterator<String> databases() {
		return dbs.listIterator(0);
	}

	private Strategy getStrategy(String name) {
		if (name.equals(".")) name = "lev";
		return strategies.get(name);
	}

	private int strategiesSize() {
		return strategies.keySet().size();
	}

	private Iterator<String> strategies() {
		return strategies.keySet().iterator();
	}

	private int matchesParsing(String matches, int positionElement) {														
		ConceptualizedElement conceptualizedElement = null;								
		String[] strings = matches.split("\n");		
		int s = 1;
		
		if (strings[0].equals(previousLemma)) {							
			conceptualizedElement = conceptualizedSet.get(conceptualizedSet.size()-1);				
			parsingForExistingElement(strings, conceptualizedElement, positionElement-1);
			s = 0;
		}
		else {	
			conceptualizedElement = new ConceptualizedElement();
			previousLemma = new String(strings[0]);								
			parsingForNewElement(strings, conceptualizedElement, positionElement);
		}
					
		if (s == 1) {				
			conceptualizedSet.add(conceptualizedElement);
			return 1;
		}
		else
			return 0;
	}
			
	public static void parsingForNewElement(String[] strings, ConceptualizedElement conceptualizedElement, int positionElement) {
	    int i = strings.length;
		String[] translations = new String[i-1];						
		int j;
		String[] splitTranslations = null; 
		for (j = 0; j < i-1; j++) {
		    translations[j] = strings[j+1].replaceFirst("\\s*","");
			if (translations[j].equals(""))
				continue;
		    //System.out.println("translationsParsingForNewElement: " + translations[j]);
		    //int k = 3;translations[j] = (strings[j+1].substring(k));			
			splitTranslations = translations[j].split("[;,] ");																	
			conceptualizedElement.setConceptualizedTranslation(strings[0], positionElement, splitTranslations);				
		}
	}
	
	public static void parsingForExistingElement(String[] strings, ConceptualizedElement conceptualizedElement, int positionElement) {	
	    int i = strings.length;
		String[] translations = new String[i-1];						
		int j;
		String[] splitTranslations = null; 
		for (j = 0; j < i-1; j++) {												
			//int k = 3;translations[j] = (strings[j+1].substring(k));
			translations[j] = strings[j+1].replaceFirst("\\s*","");
			if (translations[j].equals(""))
				continue;
			//System.out.println("translationsParsingForExistingElement: " + translations[j]);
		}
		conceptualizedElement.setConceptualizedTranslation(strings[0], positionElement, translations);
	}
	
	public void initializeDictionary(String path, String language) throws NoSuchElementException,
	IllegalAccessException, InstantiationException, LinguisticInterfaceLoadException{
		String[] args = new String[4];
		this.language = language;
		args[0] = new String("-c");
		args[1] = new String(path + "/databases.xml");				
		args[2] = new String("--strats");
		args[3] = new String(path + "/strategies.xml");
		try {
			init(args);
		} catch (IOException e) {
			throw new LinguisticInterfaceLoadException(e);
		} catch (ClassNotFoundException e) {
			throw new LinguisticInterfaceLoadException(e);
		} catch (IllegalAccessException e) {
			throw new LinguisticInterfaceLoadException(e);
		} catch (InstantiationException e) {			
			throw new LinguisticInterfaceLoadException(e);
		}
		database = getDatabase(language);
		if(database == null)
			throw new LinguisticInterfaceLoadException("The language "+language+" does not have a database associated with it");
	}
	
	public void setStrategy(String strat) {		
		try {
			strategy = (Strategy)Class.forName(strat).newInstance();
		} catch (InstantiationException e) {			
			e.printStackTrace();
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {			
			e.printStackTrace();
		}		
	}
	
	public void translateWord(String lemma) {
		String results = null;
		Matches matches = null;	
		try {
			matches = database.match(lemma, strategy);
		} catch (DatabaseException e1) {			
			e1.printStackTrace();
		}	
		int i = 0;
		int s = 0;
		if (matches!=null)
			try {
				//System.out.println("Result:");
				while (matches.hasNext()) {
					byte[] byteArray = ((String)matches.next().getContent()).getBytes();
					String result = new String(byteArray, "UTF8");
					//System.out.println(result);
					s = matchesParsing(result, i);	
					if (s == 1)
						i++;				
				}
			} catch (DatabaseException e2) {			
				e2.printStackTrace();
			} catch (UnsupportedEncodingException e) {			
				e.printStackTrace();
			}			
	}
	
	public void initializeConceptualizedSet() {
		previousLemma = null;
		conceptualizedSet = new Vector<ConceptualizedElement>();
	}
	
	public Iterator<ConceptualizedElement> getConceptualizedSet() {		
		return conceptualizedSet.iterator();
	}
		
	/*public static void main(String[] args) throws NoSuchElementException,
	IllegalAccessException, InstantiationException {		

	    String mainPath = "F:/Development/Protege_3.0/plugins/it.uniroma2.info.ai-nlp.ontoling/Dict/";
	 	FileInputStream propertyFile = null;
	 	Dict.setStrategy("ar.com.ktulu.dict.strategies.ExactStrategy");
		try {
			propertyFile = new FileInputStream(new File(mainPath+"Dict.properties"));
		} catch (FileNotFoundException e3) {			
			e3.printStackTrace();
		}
		Properties p = new Properties();
        try {
			p.load(propertyFile);
		} catch (IOException e1) {			
			e1.printStackTrace();
		}
        try {
			propertyFile.close();
		} catch (IOException e2) {			
			e2.printStackTrace();
		}
		
	 	String dictFormatLanguage = new String(p.getProperty("Language"));
	 	Dict.setLanguage(dictFormatLanguage);
	 	String split[] = dictFormatLanguage.split("-");		
	 	language = split[1].substring(0,2);
	 	
		try {			
			initializeDictionary(mainPath);
		} catch (NoSuchElementException e) {			
			e.printStackTrace();
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		} catch (InstantiationException e) {			
			e.printStackTrace();
		}
		
//		translateWord("city");

		Matches matches = null;
		try {
			matches = database.match("city", strategy);
		} catch (DatabaseException e1) {
		    System.out.println("database exception\n");
			e1.printStackTrace();
		}
		
		try {
			while (matches.hasNext()) {				
				byte[] byteArray = ((String)matches.next().getContent()).getBytes();
				//System.out.println(new String(byteArray, "UTF8"));
			}
		} catch (DatabaseException e2) {			
			e2.printStackTrace();		
		}
	
	}*/
}
