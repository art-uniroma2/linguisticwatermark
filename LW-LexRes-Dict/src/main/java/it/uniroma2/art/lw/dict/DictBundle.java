package it.uniroma2.art.lw.dict;

import it.uniroma2.art.lw.model.objects.LIFactory;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class DictBundle implements BundleActivator{

	public void start(BundleContext context) throws Exception {
		context.registerService(LIFactory.class.getName(), 
				new DictFactory("DICT"), null);		
	}

	public void stop(BundleContext arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
