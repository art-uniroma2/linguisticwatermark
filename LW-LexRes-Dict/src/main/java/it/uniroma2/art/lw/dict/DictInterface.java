 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is DictInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * DictInterface was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about DictInterface can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Donato Griesi
  */

/*
 * Created on Mar 29, 2005
 */
package it.uniroma2.art.lw.dict;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;
import it.uniroma2.art.lw.model.objects.BilingualLinguisticInterface;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchStrategy;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.ConceptPrefixSearch;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.lw.model.objects.WholeWordToggling;
import it.uniroma2.art.lw.properties.InstanceProperty;
import it.uniroma2.art.lw.properties.InterfaceProperty;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Donato Griesi, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class DictInterface extends BilingualLinguisticInterface implements ConceptualizedLR, WholeWordToggling, ConceptPrefixSearch {
	private Dict dict;		
	private Iterator<ConceptualizedElement> conceptualizedSet = null;
	private int cache = 1;	
	
	@InterfaceProperty(description="path where is dictionary.xml")
	static public String dictionary_path;
	
	@InstanceProperty(description="source language")
	public String source_language;
	
	@InstanceProperty(description="destination language")
	public String destination_language;
	
	//public DictionaryProperty dictionary_path;
	//public SourceLanguage source_language;
	//public DestinationLanguage destination_language;
	
	private Collection<SemanticRelation> semRelList;
    private Collection<LexicalRelation> lexRelList;
    private Collection<SearchStrategy> searchStratList;
    private Collection<SearchFilter> searchFilterList;
	
	
	
	public void initialize () throws LinguisticInterfaceLoadException{		
		String mainPath = dictionary_path;
		dict = new Dict();
	    sourceLanguage = source_language;
	 	language = destination_language;
	 	String dictFormatLanguage = new String(sourceLanguage + "-" + language);
		try {			
			dict.initializeDictionary(mainPath, dictFormatLanguage);
		} catch (NoSuchElementException e) {			
			e.printStackTrace();
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		} catch (InstantiationException e) {			
			e.printStackTrace();
		}
		
		setSearchStrategies();
		//setSearchFilters();
		
        //setWholeWordSearchStatus(true);
        shortID = "DICT-" + sourceLanguage + "-" + language;
	}
	
	 public void initialize(String[] args) throws LinguisticInterfaceLoadException{
	 	String mainPath = args[0];
	 	/*FileInputStream propertyFile = null;
		try {
			propertyFile = new FileInputStream(new File(mainPath+"Dict.properties"));
		} catch (FileNotFoundException e3) {
		    Dict.showErrorMsg("Property file: \"Dict.properties\" not found!");
			e3.printStackTrace();
		}
		Properties p = new Properties();
        try {
			p.load(propertyFile);
		} catch (IOException e1) {
		    Dict.showErrorMsg("Problems in parsing property file: \"Dict.properties\".");
			e1.printStackTrace();
		}
        try {
			propertyFile.close();
		} catch (IOException e2) {			
		    Dict.showErrorMsg("Problems in parsing property file: \"Dict.properties\".");
			e2.printStackTrace();
		}*/
		
	 	//String dictFormatLanguage = new String(p.getProperty("Language"));
	 	String dictFormatLanguage = new String(args[1]);
	 	dict = new Dict();	 	
	 	String split[] = dictFormatLanguage.split("-");		
	 	language = split[1].substring(0,1);
	 	sourceLanguage = split[0].substring(0,1);
	 	
		try {			
			dict.initializeDictionary(mainPath, dictFormatLanguage);
		} catch (NoSuchElementException e) {			
			e.printStackTrace();
		} catch (IllegalAccessException e) {			
			e.printStackTrace();
		} catch (InstantiationException e) {			
			e.printStackTrace();
		}
		
		setSearchStrategies();
		//setSearchFilters();
		
        //setWholeWordSearchStatus(true);
        shortID = "DICT-" + sourceLanguage + "-" + language;
	 }
	 
	 private Collection<SemanticIndex> buildConceptList() {
		 	ArrayList <SemanticIndex>result = new ArrayList<SemanticIndex>();
		 	DictConcept[] dictConceptArray = null;	 	
		 	while (conceptualizedSet.hasNext()) {
		 		ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();	 		
		 		dictConceptArray = conceptualizedElement.getConcepts();
		 		if (dictConceptArray != null) {
		 			for (int i = 0; i < dictConceptArray.length; i++) {
		 				result.add(dictConceptArray[i]);
		 			}
		 		}		
		 	}	 	
		 	return result;
		 }
	 
	 private Collection<SearchWord> buildLemmaList() {
	 	ArrayList <SearchWord>result = new ArrayList<SearchWord>();
	 	DictConcept[] dictConceptArray = null;	 	
	 	while (conceptualizedSet.hasNext()) {
	 		ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();	 		
	 		dictConceptArray = conceptualizedElement.getConcepts();
	 		if (dictConceptArray != null) {
	 			for (int i = 0; i < dictConceptArray.length; i++) {
	 				String lemma;
	 				int pos = dictConceptArray[i].getLemma().indexOf("[");
	 				if(pos != -1)
	 					lemma = dictConceptArray[i].getLemma().substring(0, pos).trim();
	 				else
	 					lemma = dictConceptArray[i].getLemma();
	 				result.add(new SearchWord(lemma));
	 			}
	 		}		
	 	}	 	
	 	return result;
	 }
	 
	 private Collection<SearchWord> searchStrategy_Translations(String term) {
	 	setCache(1);
	 	dict.initializeConceptualizedSet(); 
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.ExactStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8); 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();	 	
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_LevenshteinStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.LevenshteinStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8); 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_PrefixStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.PrefixStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_REPerl5StrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.REPerl5Strategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8); 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_REPOSIXStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.REPOSIXStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_SoundexStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.SoundexStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8); 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_SubStringStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.SubStringStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 		 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 private Collection<SearchWord> searchStrategy_SuffixStrategyTranslation(String term) {	 		 		 	
	 	setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.SuffixStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 		 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildLemmaList();
	 }
	 
	 public boolean entryExists(String entry) {
	 	Collection<SemanticIndex> concepts = getConcepts(new SearchWord(entry));
	 	Iterator <SemanticIndex>it = concepts.iterator();
	 	while (it.hasNext()) {
	 		SemanticIndex c = it.next();
	 		if (c != null) {
	 			return true;
	 		}
	 	}
	 	return false;
	 }
	 
	 public String[] getConceptLexicals(SemanticIndex c) {	 		 		 		 		 		 	
	 	String[] translations = null;	 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();	 		 		 	
	 	int i = ((DictConcept)c).getElementPositionConcept();
	 	int j = 0;	 	
		if (conceptualizedSet != null) {	 			 			 		
	 		while (conceptualizedSet.hasNext()) {	 			
	 			ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();
	 			if (i != j) {	 				
	 				j++;
	 				continue;
	 			}	 			
	 			translations = conceptualizedElement.getTranslations((DictConcept)c);
	 			if (translations != null) 
	 				return translations;	 			 			
	 			else
	 				break;
	 		}
	 	}	 		 		 	
	 	return translations;	 	
	 }
	 
	 public SemanticIndex getConcept(String conceptID) throws SemIndexRetrievalException {	 		 		 		 		 	
	 	String[] split = conceptID.split("_");
	 	DictConcept dictConcept;	 		 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	if (conceptualizedSet != null && getCacheStatus() == 1) {	 			 			 			 		
	 		while (conceptualizedSet.hasNext()) {	 				 			
	 			ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();	 			 				 				 				 				 					
	 			dictConcept = conceptualizedElement.getConceptualizationByID(split[0], new Integer(split[1]).intValue());	 				 			
	 			if (dictConcept != null) {	 				
	 				return dictConcept;
	 			}
	 		}
	 	}
	 			
	 	setCache(0);
	 	dict.initializeConceptualizedSet(); 
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.ExactStrategy");
		dict.translateWord(split[0]);
		conceptualizedSet = dict.getConceptualizedSet();
		int i = 0;
		int relativePositionConcept = new Integer(split[1]).intValue();
		if (conceptualizedSet != null) {	 			 			 			 		
			while (conceptualizedSet.hasNext()) {	 				 			
				if (relativePositionConcept == i) {
					ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();	 			 				 				 				 				 					
					dictConcept = conceptualizedElement.getConceptualizationByID(split[0], relativePositionConcept);	 				 			
					if (dictConcept != null) {	 				
						return dictConcept;
					}
				}
				else 
					i++;
			}
		}
		
	 	return null;	
	 }
	 
	 public Collection<SemanticIndex> getConcepts(SearchWord searchWord) {	 		 	
	 	setCache(1);
	 	DictConcept[] dictConceptArray = null;
	 	ArrayList<SemanticIndex> result = new ArrayList<SemanticIndex>();
	 	if(searchWord == null || searchWord.getName().compareTo("")==0)
	 		return result;
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.ExactStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = searchWord.getName().getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
		String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 					 		 		 	
		conceptualizedSet = dict.getConceptualizedSet();
	 	while (conceptualizedSet.hasNext()) {
	 		ConceptualizedElement conceptualizedElement = (ConceptualizedElement)conceptualizedSet.next();	 		
	 		dictConceptArray = conceptualizedElement.getConcepts();
	 		if (dictConceptArray != null) {
	 			for (int i = 0; i < dictConceptArray.length; i++) {
	 				result.add(dictConceptArray[i]);
	 			}
	 		}	 			
	 	}
	 	return result;
	 }
	
	private void setCache(int value) {
		cache = value;
	}
	
	private int getCacheStatus() {
		return cache;
	}

    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.WholeWordToggling#setWholeWordSearchStatus(boolean)
     */
    public void setWholeWordSearchStatus(boolean status) {
    	for(SearchFilter srchFilter : searchFilterList){
			if(srchFilter.getName().compareTo("Whole word search") == 0)
				srchFilter.setActive(status);
		}
    }
    

	/* (non-Javadoc)
	 * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.ConceptPrefixSearch#getConceptsByPrefixStrategy(java.lang.String)
	 */
	public Collection<SemanticIndex> getConceptsByPrefixStrategy(String term) {
		setCache(1);
	 	dict.initializeConceptualizedSet();
	 	dict.setStrategy("ar.com.ktulu.dict.strategies.PrefixStrategy");
	 	byte[] byteArray = null; 
	 	try {
			byteArray = term.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}	 	
	 	String termUTF8 = new String(byteArray);
		dict.translateWord(termUTF8);	 	 		 	
	 	conceptualizedSet = dict.getConceptualizedSet();
	 	return buildConceptList();		
	}

	
	private void setSearchStrategies(){
		searchStratList = new ArrayList<SearchStrategy>();
		searchStratList.add(new SearchStrategy("Translations"));
		searchStratList.add(new SearchStrategy("LevenshteinStrategyTranslation"));
		searchStratList.add(new SearchStrategy("PrefixStrategyTranslation"));
		searchStratList.add(new SearchStrategy("REPerl5StrategyTranslation"));
		searchStratList.add(new SearchStrategy("REPOSIXStrategyTranslation"));
		searchStratList.add(new SearchStrategy("SoundexStrategyTranslation"));
		searchStratList.add(new SearchStrategy("SubStringStrategyTranslation"));
		searchStratList.add(new SearchStrategy("SuffixStrategyTranslation"));
    }
	/*
	private void setSearchFilters(){
		searchFilterList = new ArrayList<SearchFilter>();
    	SearchFilter searchFilter;
    	searchFilter = new SearchFilter("Whole word search");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    }
	*/
	
	@Override
	public LexicalRelation getLexicalRelation(String lexRelation) {
		for(LexicalRelation lexRel : lexRelList){
			if(lexRel.getName().compareTo(lexRelation) == 0)
				return lexRel;
		}
		return null;
	}

	@Override
	public Collection<LexicalRelation> getLexicalRelations() {
		return lexRelList;
	}

	@Override
	public SearchFilter getSearchFilter(String searchFilter) {
		for(SearchFilter srchFilter : searchFilterList){
			if(srchFilter.getName().compareTo(searchFilter) == 0)
				return srchFilter;
		}
		return null;
	}

	@Override
	public Collection<SearchFilter> getSearchFilters() {
		return searchFilterList;
	}

	@Override
	public Collection<SearchStrategy> getSearchStrategies() {
		return searchStratList;
	}

	@Override
	public SearchStrategy getSearchStrategy(String searchStrategy) {
		for(SearchStrategy searchStrat : searchStratList){
			if(searchStrat.getName().compareTo(searchStrategy) == 0)
				return searchStrat;
		}
		return null;
	}

	@Override
	public SemanticRelation getSemanticRelation(String semRelation) {
		for(SemanticRelation semRel : semRelList){
			if(semRel.getName().compareTo(semRelation) == 0)
				return semRel;
		}
		return null;
	}

	@Override
	public Collection<SemanticRelation> getSemanticRelations() {
		return semRelList;
	}

	@Override
	protected Collection<SearchWord> getSearchWords(String word,
			String methodName, SearchFilter... searchFilters) {
		if(methodName.compareTo("Translations") == 0)
			return this.searchStrategy_Translations(word);
		else if(methodName.compareTo("LevenshteinStrategyTranslation") == 0)
			return this.searchStrategy_LevenshteinStrategyTranslation(word);
		else if(methodName.compareTo("PrefixStrategyTranslation") == 0)
			return this.searchStrategy_PrefixStrategyTranslation(word);
		else if(methodName.compareTo("REPerl5StrategyTranslation") == 0)
			return this.searchStrategy_REPerl5StrategyTranslation(word);
		else if(methodName.compareTo("REPOSIXStrategyTranslation") == 0)
			return this.searchStrategy_REPOSIXStrategyTranslation(word);
		else if(methodName.compareTo("SoundexStrategyTranslation") == 0)
			return this.searchStrategy_SoundexStrategyTranslation(word);
		else if(methodName.compareTo("SubStringStrategyTranslation") == 0)
			return this.searchStrategy_SubStringStrategyTranslation(word);
		else if(methodName.compareTo("SuffixStrategyTranslation") == 0)
			return this.searchStrategy_SuffixStrategyTranslation(word);
			
		return null;
	}

	@Override
	public Collection<SemanticIndex> exploreLexicalRelation(String word,
			SemanticIndex c, LexicalRelation Relation)
			throws LinguisticResourceAccessException {
		return null;
	}

	@Override
	public Collection<SemanticIndex> exploreSemanticRelation(SemanticIndex c,
			SemanticRelation Relation) throws LinguisticResourceAccessException {
		return null;
	}

}
