package it.uniroma2.art.lw.dict;

import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;

public class DictFactory extends LIFactory{

	public DictFactory(String id) {
		super(id, DictInterface.class);
	}

	public LinguisticInterface getLinguisticInterface() {
		DictInterface dictInterface = new DictInterface();
		return dictInterface;
	}

	

}
