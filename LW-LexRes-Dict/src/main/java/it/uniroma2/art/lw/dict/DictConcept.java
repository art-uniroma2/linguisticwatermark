 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is DictInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * DictInterface was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about DictInterface can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Donato Griesi
  */

/*
 * Created on Mar 30, 2005
 */
package it.uniroma2.art.lw.dict;

import it.uniroma2.art.lw.model.objects.SemanticIndex;

/**
 * @author Donato Griesi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DictConcept extends SemanticIndex {
	private String lemma = null;
	private int elementPositionConcept = 0; 
	private int relativePositionConcept = 0;
	
	
	public DictConcept(String l, int elementPosition, int relativePosition) {
		lemma = l;
		elementPositionConcept = elementPosition;
		relativePositionConcept = relativePosition;		
	}
	
	public String getConceptRepresentation() {		
		return (lemma + "_" + new Integer(relativePositionConcept).toString());
	}
		
	public int getElementPositionConcept() {
		return elementPositionConcept;
	}
	
	public int getRelativePositionConcept() {
		return relativePositionConcept;
	}
	
	public String getLemma() {
		return lemma;
	}
}
