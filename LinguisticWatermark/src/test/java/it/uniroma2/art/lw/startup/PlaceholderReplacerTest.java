/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is Linguistic Watermark.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
 * All Rights Reserved.
 *
 * Linguistic Watermark was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
 * Current information about Linguistic Watermark can be obtained at 
 * http://art.uniroma2.it/software/LinguisticWatermark/
 *
 */

package it.uniroma2.art.lw.startup;

import static org.junit.Assert.*;

import it.uniroma2.art.lw.exceptions.ReplacePlaceholderException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PlaceholderReplacerTest {

	@Test
	public void testPlaceholderReplacer() throws ReplacePlaceholderException {

		Map<String, String> placeholderValueMap = new HashMap<String, String>();
		PlaceholderReplacer placeholderReplace;
		String testJarInputFilePath = "./it/uniroma2/art/lw/startup/LWConfigInput.xml";
		String configInputFilePath = "./testData/temp/LWConfigInput.xml";
		String configOutputFilePath = "./testData/temp/LWConfigOutput.xml";
		inputStreamToFile(ClassLoader.getSystemClassLoader().getResourceAsStream(testJarInputFilePath), new File(configInputFilePath));		
		
		placeholderValueMap.put("lrpath", "./testData");

		placeholderReplace = new PlaceholderReplacer(placeholderValueMap, configInputFilePath,
				configOutputFilePath);

		placeholderReplace.replacePlaceholders();
	}

	public static void inputStreamToFile(InputStream inputStream, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
		} catch (IOException e) {
		}
	}


}
