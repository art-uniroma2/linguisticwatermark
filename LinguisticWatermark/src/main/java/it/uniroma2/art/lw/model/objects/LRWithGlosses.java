 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is LinguisticWatermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * LinguisticWatermark was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about LinguisticWatermark can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.lw.model.objects;

/**
 * this interface must be implemented by wrapper for those resources which admit glosses for describing
 * the different senses which words may have.
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public interface LRWithGlosses extends ConceptualizedLR {
    /**
     * @param c a word(s') sense
     * @return the gloss associated to sense c
     */
    public String getConceptGloss(SemanticIndex c);
    
    public String getWord(SemanticIndex c);
}
