 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.model;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.model.store.LingModelIO;
import it.uniroma2.art.lw.model.store.xml.LingModelXMLIO;
import it.uniroma2.art.lw.plugin.LWOSGIManager;

import java.io.File;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class LingModelFactory {

	/**
     * @param oSGiDir the directory used by Felix for caching extension extracted from OSGi bundles
     * @param rootBundleDir the root directory where to look for the bundles
     * @param subDirPresent true if there are subdirectory where to look for bundle, false otherwise
     * @param relSubDir the string representing the relative path in where the bundle are if there are subdirectories
     * @param configFile the configuration file
     * @return
     * @throws PMInitializationException 
     * @throws PMInitializationException
     * @throws LinguisticInterfaceLoadException 
     * @throws LingModelReadException 
     * @throws LingModelReadException 
     * @throws LinguisticInterfaceLoadException 
     */
    public static LingModel initializeLingModel(File oSGiDir, String rootBundleDir, boolean  subDirPresent, 
    		File configFile) throws PMInitializationException, LingModelReadException, LinguisticInterfaceLoadException {      
        
    	return initializeLingModel(oSGiDir, rootBundleDir, subDirPresent, configFile, true);
    }
	
    /**
     * @param oSGiDir the directory used by Felix for caching extension extracted from OSGi bundles
     * @param rootBundleDir the root directory where to look for the bundles
     * @param subDirPresent true if there are subdirectory where to look for bundle, false otherwise
     * @param relSubDir the string representing the relative path in where the bundle are if there are subdirectories
     * @param configFile the configuration file
	 * @param withAWT 
     * @return
     * @throws PMInitializationException 
     * @throws PMInitializationException
     * @throws LinguisticInterfaceLoadException 
     * @throws LingModelReadException 
     * @throws LingModelReadException 
     * @throws LinguisticInterfaceLoadException 
     */
    public static LingModel initializeLingModel(File oSGiDir, String rootBundleDir, boolean  subDirPresent, 
    		File configFile, boolean withAWT) throws PMInitializationException, LingModelReadException, LinguisticInterfaceLoadException {      
        
    	LWOSGIManager.initialize(oSGiDir);
        
        //TODO ora relSubDir e/o rootBundleDir dovrebbero avere valori diversi per i vari punti di estensione, quindi questo � un problema
		
        LWOSGIManager.getManager().loadEnrichMethods(rootBundleDir, subDirPresent);
                
        LWOSGIManager.getManager().loadFactoryLW(rootBundleDir, subDirPresent);
        
        LWOSGIManager.getManager().removeOldBundle();
        
        LingModel lm = new LingModel(LWOSGIManager.getManager().getLinguisticInterfaceFactoryList());
        
        LingModelIO lmIO = new LingModelXMLIO(configFile);
        
        lmIO.populateLingModel(lm, withAWT);
        
        lm.setLMIO(lmIO);
        
        return lm;
    } 
    
}
