 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.model;

import java.util.Collection;
import java.util.HashMap;

import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.model.objects.LIFactory;

public class LinguisticResourceType {
	private String id;
	private LIFactory linguisticInterfaceFactory;
	private HashMap<String, String> interfaceProperties; 
	 
	public LinguisticResourceType(String id){
		this.id = id;
		linguisticInterfaceFactory = null;
		interfaceProperties = new HashMap<String, String>();
	}
	
	/**
	 * 
	 * @return the id of the LIFactory that is (or should be) in the resouce
	 */
	public String getId(){
		return id;
	}
	
	public void setLIFactory(LIFactory liFacotry){
		linguisticInterfaceFactory = liFacotry;
	}
	
	/**
	 * 
	 * @return the LIFactory or null if there is no LIFactory associated
	 */
	public LIFactory getLIFactory(){
		return linguisticInterfaceFactory;
	}
	
	/**
	 * 
	 * @param propId name of the property
	 * @param value value of the property
	 * @throws PropertyNotFoundException
	 */
	public void setProperty(String propId, String value) throws PropertyNotFoundException{
		if(interfaceProperties.containsKey(propId)){
			interfaceProperties.remove(propId);
    	}
		interfaceProperties.put(propId, value);
		
		if(linguisticInterfaceFactory != null){
			linguisticInterfaceFactory.setInterfaceProperty(propId, value);
		}
	}
	
	/**
	 * 
	 * @return all the names of the interface properties
	 */
	public Collection<String> getInterfaceProperties(){
		return interfaceProperties.keySet();
	}
	
	
	/**
	 * 
	 * @param propId name of the property 
	 * @return value of the property identified by propId
	 * @throws PropertyNotFoundException
	 */
	public String getPropertyValue(String propId) throws PropertyNotFoundException {
    	String value = interfaceProperties.get(propId);
    	if(value != null){
    		return value;
    	}
    	else{
    		throw new PropertyNotFoundException("property "+propId+" of "+this.getId()+" not found");
    	}
	}
	
	/**
	 * 
	 * @param propId
	 * @return description of the property or a empty string if there is non LIFactory in the resource
	 * @throws PropertyNotFoundException
	 */
	public String getPropertyDescription(String propId) throws PropertyNotFoundException {
		if(linguisticInterfaceFactory != null){
			return linguisticInterfaceFactory.getPropertyDescription(propId);
		}
		return "";
	}
	

}
