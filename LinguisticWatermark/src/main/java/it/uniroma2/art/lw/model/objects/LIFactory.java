package it.uniroma2.art.lw.model.objects;

import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.properties.DirectoryProperty;
import it.uniroma2.art.lw.properties.FileProperty;
import it.uniroma2.art.lw.properties.InstanceProperty;
import it.uniroma2.art.lw.properties.InterfaceProperty;
import it.uniroma2.art.lw.plugin.LWOSGIExtension;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

public abstract class LIFactory implements LWOSGIExtension{
	private String id;	
	protected Class<? extends LinguisticInterface> lingIntCls;
	
	public LIFactory(String id, Class<? extends LinguisticInterface> lingIntCls){
		this.id = id;
		this.lingIntCls = lingIntCls;
	}
		
	
	public String getId(){
		return id;
	}

	/**
	 * @return an instance of a LinguisticInterface
	 */
	public abstract LinguisticInterface getLinguisticInterface();
	
	/**
	 * @return the collection of InterfaceProperties for the LinguisticInterface class managed by this Factory
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Collection<String> getInterfaceProperties() {
	    Collection<String> interfaceProperties = new ArrayList<String>();
	    Field[] fields = lingIntCls.getFields();
	    for (Field field : fields)
	        if (field.isAnnotationPresent(InterfaceProperty.class))
	            interfaceProperties.add(field.getName());
	    return interfaceProperties;
	}

	/**
	 * sets the value of the Interface Property for the Linguistic Interface managed by this factory
	 * 
	 * @param id
	 * @param value
	 * @throws PropertyNotFoundException
	 */
	public void setInterfaceProperty(String id, String value) throws PropertyNotFoundException {
	    try {
            Field prop = lingIntCls.getField(id);
            prop.set(null, value);
        } catch (SecurityException e) {
            throw new PropertyNotFoundException(e);
        } catch (NoSuchFieldException e) {
            throw new PropertyNotFoundException(e);
        } catch (IllegalArgumentException e) {
            throw new PropertyNotFoundException(e);
        } catch (IllegalAccessException e) {
            throw new PropertyNotFoundException(e);
        }        
	}	

	
    public String getPropertyValue(String id) throws PropertyNotFoundException {
        try {
            return (String)lingIntCls.getField(id).get(null);
        } catch (SecurityException e) {
            throw new PropertyNotFoundException(e);
        } catch (NoSuchFieldException e) {
            throw new PropertyNotFoundException(e);
        } catch (IllegalArgumentException e) {
            throw new PropertyNotFoundException(e);
        } catch (IllegalAccessException e) {
            throw new PropertyNotFoundException(e);
        }
    }
    
	
	public String getPropertyDescription(String id) throws PropertyNotFoundException {
	    try {
	        Field field = lingIntCls.getField(id);
	        if (field.isAnnotationPresent(InterfaceProperty.class))
	            return ((InterfaceProperty)field.getAnnotation(InterfaceProperty.class)).description();
	        else if (field.isAnnotationPresent(InstanceProperty.class))
	            return ((InstanceProperty)field.getAnnotation(InstanceProperty.class)).description();
	        else throw new PropertyNotFoundException("Property: " + id + " is present in the Linguistic Interface but is not declared as an Interface Property nor an Instace Property");
        } catch (SecurityException e) {
            throw new PropertyNotFoundException(e);
        } catch (NoSuchFieldException e) {
            throw new PropertyNotFoundException(e);
        }
	}
	
	
	/**
	 * @return the collection of InstanceProperties for the LinguisticInterface class managed by this Factory
	 */
	public Collection<String> getInstanceProperties() {
        Collection<String> instanceProperties = new ArrayList<String>();
        Field[] fields = lingIntCls.getFields();
        for (Field field : fields)
            if (field.isAnnotationPresent(InstanceProperty.class))
                instanceProperties.add(field.getName());
        return instanceProperties;
    }
		
	
	public Class<? extends LinguisticInterface> getLinguisticInterfaceClass() {
	    return lingIntCls;
	}


	public boolean isAPropertyFile(String propID) throws PropertyNotFoundException {
		try {
	        Field field = lingIntCls.getField(propID);
	        if (field.isAnnotationPresent(FileProperty.class))
	            return true;
	    } catch (SecurityException e) {
            throw new PropertyNotFoundException(e);
        } catch (NoSuchFieldException e) {
            throw new PropertyNotFoundException(e);
        }
		return false;
	}
	
	public boolean isAPropertyDirectory(String propID) throws PropertyNotFoundException {
		try {
	        Field field = lingIntCls.getField(propID);
	        if (field.isAnnotationPresent(DirectoryProperty.class))
	            return true;
	    } catch (SecurityException e) {
            throw new PropertyNotFoundException(e);
        } catch (NoSuchFieldException e) {
            throw new PropertyNotFoundException(e);
        }
		return false;
	}
}
