package it.uniroma2.art.lw.test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LingModelFactory;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.LinguisticResourceType;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.lw.model.objects.TaxonomicalLR;

public class FelixXmlTest_WordNet {

	File oSGiDir = null;
	String rootDirBundle = null;
	File configFile = null;
	LingModel lingModel = null;
	private LinguisticInterface lIntWORDNET;
	private Collection<SemanticRelation> semRelList;
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		FelixXmlTest_WordNet felixXmlTest_WordNet = new FelixXmlTest_WordNet();
		
		felixXmlTest_WordNet.initialize();
		
		felixXmlTest_WordNet.startTest();
		

	}
	
	
	public FelixXmlTest_WordNet() {
		
	}
	
	public void initialize() {
		oSGiDir = new File("oSGiDir");
		rootDirBundle = "rootBundleDir";
		configFile = new File(
				"config/LWConfig_WordNet.xml");

		try {
			lingModel = LingModelFactory.initializeLingModel(oSGiDir, rootDirBundle, false, configFile, false);

			System.out.println("Felix è stato avviato e file XMl è stato letto");


			// Test di Wordnet_2.0
			// fare attenzione ai vari percorsi all'interno dei file di configurazione

			System.out.println("\n*****************************************");
			System.out.println("Test di WORDNET WordNet_2.1");
			System.out.println("******************************************\n");
			
			LinguisticResource lResWORNDET = lingModel.getLinguisticResource("WordNet_2.1");
			lIntWORDNET = lResWORNDET.loadLinguisticInterface();
			
			// LinguisticInterface lIntWORDNET = lResWORNDET.getLinguisticInterface();
			//lIntWORDNET.initialize();
			
		} catch (PMInitializationException e) {
			e.printStackTrace();
		} catch (LingModelReadException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (LinguisticInterfaceLoadException e) {
			e.printStackTrace();
		}
	}
	
	
	public void startTest(){
		try {
			
			System.out.println("LexicalRelation in WordNet:");
			Collection<LexicalRelation> lexRelList = lIntWORDNET.getLexicalRelations();
			for(LexicalRelation lexRel : lexRelList){
				System.out.println(lexRel.getName());
			}
			
			System.out.println("\nSemanticRelation in WordNet:");
			semRelList = lIntWORDNET.getSemanticRelations();
			for(SemanticRelation semRel : semRelList){
				System.out.println(semRel.getName());
			}
			
			
			
			String termWordnet = "cat";
			Collection<SemanticIndex> resultsWordnet = ((ConceptualizedLR) lIntWORDNET)
					.getConcepts(new SearchWord(termWordnet));
			System.out.println("\n\tSearched \"" + termWordnet + "\" on WordNet:");
			boolean getSemRel = true;
			String tab = "\t\t";
			for (SemanticIndex conceptWordnet : resultsWordnet) {
				
				printSemanticIndex(conceptWordnet, tab, lIntWORDNET, getSemRel);
				
				getSemRel = false; 
				
				/*if(first){
					first = false;
					Collection<SemanticIndex> ancestorList = ((TaxonomicalLR)lIntWORDNET).getDirectAncestors(conceptWordnet);
					Collection<SemanticIndex> descendantList = ((TaxonomicalLR)lIntWORDNET).getDirectDescendants(conceptWordnet);
					
					System.out.println("\n"+tab+"ancestors");
					for(SemanticIndex ancestor : ancestorList){
						printSemanticIndex(ancestor, tab+tab, lIntWORDNET, false);
					}
					
					System.out.println("\n"+tab+"descendents");
					for(SemanticIndex descendent : descendantList){
						printSemanticIndex(descendent, tab+tab, lIntWORDNET, false);
					}
				}*/
				
			}

			System.out.println("unloading LinguisticInterface");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
			lingModel.unloadLoadedInstances();
			System.out.println("LinguisticInterface unloaded");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");


		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (LinguisticResourceAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void printSemanticIndex(SemanticIndex semanticIndex, String tab, LinguisticInterface lIntWORDNET,
			boolean printSemRel) throws LinguisticResourceAccessException{
		System.out.print("\n"+tab + semanticIndex.getConceptRepresentation());
		String[] stringheWornet = ((ConceptualizedLR) lIntWORDNET).getConceptLexicals(semanticIndex);
		System.out.print("\n"+tab+"Syn: ");
		for (String synWordnet : stringheWornet) {
			System.out.print(synWordnet + " / ");
		}
		
		String glossWordnet = ((LRWithGlosses) lIntWORDNET).getConceptGloss(semanticIndex);
		System.out.print("\n"+tab+"Gloss: " + glossWordnet + "\n\n");
		
		
		if(!printSemRel)
			return;
		
		for(SemanticRelation semRel : semRelList){
			Collection<SemanticIndex> linkedSynsetList = lIntWORDNET.exploreSemanticRelation(semanticIndex, semRel);
			System.out.println("\n"+tab+semRel.getName());
			for(SemanticIndex linkedSynset : linkedSynsetList){
				printSemanticIndex(linkedSynset, tab+tab, lIntWORDNET, false);
			}
		}
	}

}
