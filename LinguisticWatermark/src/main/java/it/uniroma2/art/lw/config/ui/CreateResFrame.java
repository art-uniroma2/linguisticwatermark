/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is Linguistic Watermark.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
 * All Rights Reserved.
 *
 * Linguistic Watermark was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
 * Current information about Linguistic Watermark can be obtained at 
 * http://art.uniroma2.it/software/LinguisticWatermark/
 *
 */

package it.uniroma2.art.lw.config.ui;

import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.objects.LIFactory;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CreateResFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5462296847983601151L;

	private HashMap<String, JTextField> jtextfiledMap;

	private JTextField textfieldResId;
	private ConfigureFrame parentFrame;

	public CreateResFrame() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				parentFrame.enableFrame(true);
				dispose();
			}
		});
	}

	public void initialize(final ConfigureFrame parentFrame, final LingModel lingModel,
			final String interfaceId) {
		this.parentFrame = parentFrame;
		JPanel panelGlobal = new JPanel();
		BoxLayout boxLayout = new BoxLayout(panelGlobal, BoxLayout.Y_AXIS);
		GridBagLayout instanceBag = new GridBagLayout();
		panelGlobal.setLayout(boxLayout);
		jtextfiledMap = new HashMap<String, JTextField>();
		GridBagConstraints limInstance = new GridBagConstraints();
		int instance_Y = 0;
		try {

			
			// LinguisticResource lRes =
			// lingModel.getLinguisticResource(instanceId);

			JLabel labelInstanceId = new JLabel("Instance ID = ");
			labelInstanceId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
			limInstance.anchor = GridBagConstraints.WEST;
			limInstance.fill = GridBagConstraints.NONE;
			limInstance.gridx = 0;
			limInstance.gridy = instance_Y;
			limInstance.weightx = 1;
			limInstance.gridwidth = 1;
			limInstance.gridheight = 1;
			instanceBag.setConstraints(labelInstanceId, limInstance);
			panelGlobal.add(labelInstanceId);

			textfieldResId = new JTextField();
			textfieldResId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
			limInstance.anchor = GridBagConstraints.WEST;
			limInstance.fill = GridBagConstraints.HORIZONTAL;
			limInstance.gridx = 2;
			limInstance.gridy = instance_Y;
			limInstance.weightx = 1;
			limInstance.gridwidth = 1;
			limInstance.gridheight = 1;
			instanceBag.setConstraints(textfieldResId, limInstance);
			panelGlobal.add(textfieldResId);
			instance_Y++;

			LIFactory liFactory = lingModel.getLinguisticResourceType(interfaceId).getLIFactory();
			Collection<String> instancePropertiesList = liFactory.getInstanceProperties();
			for (String instancePropertyId : instancePropertiesList) {
				JLabel labelPropId = new JLabel(instancePropertyId + ":");
				limInstance.anchor = GridBagConstraints.NORTHWEST;
				limInstance.fill = GridBagConstraints.HORIZONTAL;
				limInstance.gridx = 0;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 1;
				limInstance.weighty = 1;
				limInstance.gridwidth = 1;
				limInstance.gridheight = 1;
				instanceBag.setConstraints(labelPropId, limInstance);
				panelGlobal.add(labelPropId);

				Component strut = Box.createHorizontalGlue();
				limInstance.gridx = 1;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 1;
				instanceBag.setConstraints(strut, limInstance);
				panelGlobal.add(strut);

				JTextField textFieldPropValue = new JTextField();
				textFieldPropValue.setToolTipText(liFactory.getPropertyDescription(instancePropertyId));
				limInstance.gridx = 2;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 2;
				instanceBag.setConstraints(textFieldPropValue, limInstance);
				panelGlobal.add(textFieldPropValue);
				jtextfiledMap.put(instancePropertyId, textFieldPropValue);

				if (liFactory.isAPropertyFile(instancePropertyId)
						|| liFactory.isAPropertyDirectory(instancePropertyId)) {
					JButton button = nameFieldCreate(this, textFieldPropValue, 0);
					limInstance.gridx = 3;
					limInstance.gridy = instance_Y;
					limInstance.weightx = 2;
					instanceBag.setConstraints(button, limInstance);
					panelGlobal.add(button);
				}
				instance_Y++;
			}

		} catch (PropertyNotFoundException ex) {
			ex.printStackTrace();
		}
		JButton buttonOk = new JButton("OK");
		buttonOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (textfieldResId.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "You need to input an id for the instance");
						return;
					}
					String instanceId = textfieldResId.getText();
					LinguisticResource lRes = lingModel.getLinguisticResource(instanceId);
					if (lRes != null) {
						JOptionPane.showMessageDialog(null,
								"cannot create a Linguistic Resource with the following id: " + instanceId
										+ " because there is already one");
						return;
					}
					lRes = new LinguisticResource(instanceId);
					lRes.setLinguisticInterfaceID(interfaceId);
					Collection<LIFactory> liFactories = lingModel.getLIFactoriesLoaded();
					boolean found = false;
					for (LIFactory liFactory : liFactories) {
						if (liFactory.getId().equals(interfaceId)) {
							found = true;
							lRes.setLinguisticInterfaceFactory(liFactory);
							lRes.setLinguisticInterfaceID(interfaceId);
							lingModel.addLinguisticResourse(lRes);
							LinguisticWatermarkManager.save();
							break;
						}
					}
					if (found == false) {
						JOptionPane.showMessageDialog(null,
								"cannot create a Linguistic Resource with the following id: " + instanceId
										+ " because the interface " + interfaceId + "does not exist");
						return;
					}

					Collection<String> propertiesList = jtextfiledMap.keySet();

					lRes = lingModel.getLinguisticResource(instanceId);
					for (String propId : propertiesList) {
						JTextField textFieldPropValue = jtextfiledMap.get(propId);

						lRes.getPropertyValue(propId); // this is used only to see
											// if the property exist
						lRes.setProperty(propId, textFieldPropValue.getText());
					}

				} catch (PropertyNotFoundException e) {
					e.printStackTrace();
				}
				LinguisticWatermarkManager.save();
				dispose();
				parentFrame.enableFrame(true);

			}
		});
		limInstance.gridx = 1;
		limInstance.gridy = instance_Y;
		limInstance.weightx = 2;
		instanceBag.setConstraints(buttonOk, limInstance);
		panelGlobal.add(buttonOk);
		
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				parentFrame.enableFrame(true);
			}
		});
		limInstance.gridx = 2;
		limInstance.gridy = instance_Y;
		limInstance.weightx = 2;
		instanceBag.setConstraints(buttonCancel, limInstance);
		panelGlobal.add(buttonCancel);
		
		this.getContentPane().add(panelGlobal);
		pack();
		setSize(400, 300);
	}

	private JButton nameFieldCreate(final Component frame, final JTextField textField,
			final int directory_flag) {
		Icon icon = new ImageIcon("plugins/it.uniroma2.art.ontoling/properties/TreeClosed.gif");
		JButton nameField = new JButton(icon);
		nameField.setSize(icon.getIconHeight(), icon.getIconWidth());
		Action findAction = new AbstractAction("FindFile") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5265814998216259193L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("./"));
				if (directory_flag == 1)
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = chooser.showOpenDialog(frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File chosen = chooser.getSelectedFile();
					textField.setText(chosen.toString());
				}
			}
		};
		nameField.addActionListener(findAction);
		return nameField;
	}
}
