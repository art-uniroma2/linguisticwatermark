 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/lw/linguisticwatermark
  *
  */

package it.uniroma2.art.lw.model.store;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LingModelStoreException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.model.LingModel;

import java.io.File;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public interface LingModelIO {

	public void setFile(File file);
	
    public void storeLingModel(LingModel lm, LingModelIOParameters pars) throws LingModelStoreException;
    
    public void populateLingModel(LingModel lm) throws LingModelReadException, LinguisticInterfaceLoadException;
    
    public void populateLingModel(LingModel lm, boolean useAWT) throws LingModelReadException, LinguisticInterfaceLoadException;
}
