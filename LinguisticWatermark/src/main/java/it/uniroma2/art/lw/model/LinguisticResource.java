 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.model;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;

public class LinguisticResource {
    
    private String linguisticInterfaceID;
    private LIFactory linguisticInterfaceFactory = null;
    private LinguisticInterface linguisticInterface;
    private HashMap<String, String> instanceProperties;
    private String id;
    
    
    /**
     * constructor with default modifier (only the LingModelFactory can instantiate resources in the LingModel)
     */
    public LinguisticResource(String id) {
        this.id = id;
        this.instanceProperties = new HashMap<String, String>();
    }  
    
    /**
     * @return the linguisticInterfaceFactory or null if there is non LIFactory associated to this resource
     */
    public LIFactory getLinguisticInterfaceFactory() {
        return linguisticInterfaceFactory;
    }
    /**
     * @param linguisticInterfaceFactory the linguisticInterfaceFactory to set
     */
    public void setLinguisticInterfaceFactory(LIFactory linguisticInterfaceFactory) {
        this.linguisticInterfaceFactory = linguisticInterfaceFactory;
        for(String propId : this.linguisticInterfaceFactory.getInstanceProperties()) {
        	instanceProperties.put(propId, "");
        }
        this.setLinguisticInterfaceID(linguisticInterfaceFactory.getId());
    }
    
    public LinguisticInterface loadLinguisticInterface() throws LinguisticInterfaceLoadException{
    	if(isLoadable()){
    		linguisticInterface = linguisticInterfaceFactory.getLinguisticInterface();
    		for(String propId : instanceProperties.keySet()){
    			try {
					Field fieldProp = linguisticInterface.getClass().getField(propId);
					fieldProp.set(linguisticInterface, instanceProperties.get(propId));
				} catch (SecurityException e) {
					throw new LinguisticInterfaceLoadException(e);
				} catch (NoSuchFieldException e) {
					throw new LinguisticInterfaceLoadException(e);
				} catch (IllegalArgumentException e) {
					throw new LinguisticInterfaceLoadException(e);
				} catch (IllegalAccessException e) {
					throw new LinguisticInterfaceLoadException(e);
				}
    			
    		}
    		linguisticInterface.initialize();
    	}
    	else{
    		throw new LinguisticInterfaceLoadException("LinguisticItnerface"+id+" could not be loaded because there is no Factory "+this.linguisticInterfaceID);
    	}
    	
    	return linguisticInterface;
    }
    
    /**
     * @return the lingInt or null if the LinguisticInterface is not loaded
     */
    public LinguisticInterface getLinguisticInterface() {
        return linguisticInterface;
    }
    
    public void unloadLinguisticInterface(){
    	linguisticInterface = null;
    }
    
    /**
     * @param lingInt the lingInt to set
     */
    //public void setLinguisticInterface(LinguisticInterface lingInt) {
    //    this.linguisticInterface = lingInt;
    //}
    
    /**
     * @return the id of the configured Linguistic Resource
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id the new id for Linguistic Resource to be configured
     */
    public void setId(String id) {
        this.id=id;
    }
    
    public void setProperty(String propId, String value){ // throws PropertyNotFoundException {
    	if(instanceProperties.containsKey(propId)){
    		instanceProperties.remove(propId);
    	}
		instanceProperties.put(propId, value);
		
    }

    public String getPropertyValue(String propId) throws PropertyNotFoundException {
    	String value = instanceProperties.get(propId);
    	if(value != null){
    		return value;
    	}
    	else{
    		throw new PropertyNotFoundException("property "+propId+" of "+this.getId()+" not found");
    	}
    }
    
    public String getPropertyDescription(String propId) throws PropertyNotFoundException {
        if (linguisticInterfaceFactory!=null)
            return linguisticInterfaceFactory.getPropertyDescription(propId);
        else
        	return "";
        	//throw new PropertyNotFoundException("property "+propId+" of "+this.getId()+" not found");
    }
    

    public Collection<String> getProperties() {
    	return instanceProperties.keySet();
        //return linguisticInterfaceFactory.getInstanceProperties();
    }

    public String getLinguisticInterfaceID() {
        return linguisticInterfaceID;
    }

    public void setLinguisticInterfaceID(String linguisticInterfaceID) {
        this.linguisticInterfaceID = linguisticInterfaceID;
    }  
    
    public boolean isLoadable(){
    	if(linguisticInterfaceFactory == null)
    		return false;
    	return true;
    	
    }
    
}
