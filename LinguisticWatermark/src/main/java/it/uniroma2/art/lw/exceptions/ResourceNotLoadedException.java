/*
 * Created on 19-mag-2005
 */
package it.uniroma2.art.lw.exceptions;

/**
 * @author Donato
 */
public class ResourceNotLoadedException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3069800053459531676L;
	public ResourceNotLoadedException(String msg) {
        super(msg);
    }
	public ResourceNotLoadedException() {
        super("Resource could not be loaded!");
    }
}
