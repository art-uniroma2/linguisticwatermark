package it.uniroma2.art.lw.test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LingModelFactory;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.TaxonomicalLR;

public class FelixXmlTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Logger s_logger = Logger.getLogger(FelixXmlTest.class);

		//File oSGiDir = new File("C:/Programmi/Protege_3.3.1/plugins/it.uniroma2.info.ai-nlp.ontoling/Felix");
		File oSGiDir = new File("oSGiDir");
		// ArrayList <File>dirList = new ArrayList<File>();
		// dirList.add(new
		// File("C:/Programmi/Protege_3.3.1/plugins/it.uniroma2.info.ai-nlp.ontoling/bundle"));
		//String rootDirBundle = "C:/Programmi/Protege_3.3.1/plugins/it.uniroma2.info.ai-nlp.ontoling/bundle";
		String rootDirBundle = "rootBundleDir";
		File configFile = new File(
				"config/LWConfig.xml");
		// File configFile = new
		// File("C:/Programmi/Protege_3.3.1/plugins/it.uniroma2.info.ai-nlp.ontoling/properties/LWConfig.xml");

		LingModel lingModel = null;
		// s_logger.info("prova");

		try {
			lingModel = LingModelFactory.initializeLingModel(oSGiDir, rootDirBundle, false, configFile, false);

			System.out.println("Felix è stato avviato e file XMl è stato letto");

			// Test di Dict eng-ita
			lingModel.unloadLoadedInstances();
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");

			System.out.println("\n*****************************************");
			System.out.println("Test di Dict eng-ita");
			System.out.println("******************************************\n");
			LinguisticResource lResEngIta = lingModel.getLinguisticResource("eng-ita");
			LinguisticInterface lIntDictEngIta = lResEngIta.loadLinguisticInterface();
			// LinguisticInterface lIntDictEngIta = lResEngIta.getLinguisticInterface();
			lIntDictEngIta.initialize();

			// System.out.println(lIntEngIta.isConceptualized());
			// System.out.println(lIntEngIta.hasGlosses());
			String termDict = "window";
			Collection<SemanticIndex> resultsDict = ((ConceptualizedLR) lIntDictEngIta)
					.getConcepts(new SearchWord(termDict));
			System.out.println("\n\tSearched \"" + termDict + "\" on Dict eng-ita:");
			for (SemanticIndex conceptDict : resultsDict) {
				System.out.print("\t\t" + conceptDict.getConceptRepresentation());
				String[] stringheDict = ((ConceptualizedLR) lIntDictEngIta).getConceptLexicals(conceptDict);
				System.out.print("\n\t\tSyn: ");
				for (String synDict : stringheDict) {
					System.out.print(synDict + " / ");
				}
				System.out.print("\n\n");
			}
			System.out.println("unloading LinguisticInterface");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
			lingModel.unloadLoadedInstances();
			System.out.println("LinguisticInterface unloaded");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");

			// Test di Wordnet_2.0
			// fare attenzione ai vari percorsi all'interno dei file di configurazione

			System.out.println("\n*****************************************");
			System.out.println("Test di WORDNET WordNet_2.0");
			System.out.println("******************************************\n");

			LinguisticResource lResWORNDET = lingModel.getLinguisticResource("WordNet_2.0");
			LinguisticInterface lIntWORDNET = lResWORNDET.loadLinguisticInterface();
			// LinguisticInterface lIntWORDNET = lResWORNDET.getLinguisticInterface();
			lIntWORDNET.initialize();

			String termWordnet = "cat";
			Collection<SemanticIndex> resultsWordnet = ((ConceptualizedLR) lIntWORDNET)
					.getConcepts(new SearchWord(termWordnet));
			System.out.println("\n\tSearched \"" + termWordnet + "\" on WordNet:");
			for (SemanticIndex conceptWordnet : resultsWordnet) {
				System.out.print("\t\t" + conceptWordnet.getConceptRepresentation());
				// String gloss = ((LRWithGlosses)lResWORNDET).getConceptGloss(concept);
				String[] stringheWornet = ((ConceptualizedLR) lIntWORDNET).getConceptLexicals(conceptWordnet);
				System.out.print("\n\t\tSyn: ");
				for (String synWordnet : stringheWornet) {
					System.out.print(synWordnet + " / ");
				}
				String glossWordnet = ((LRWithGlosses) lIntWORDNET).getConceptGloss(conceptWordnet);
				System.out.print("\n\t\tGloss: " + glossWordnet + "\n\n");

			}

			System.out.println("unloading LinguisticInterface");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
			lingModel.unloadLoadedInstances();
			System.out.println("LinguisticInterface unloaded");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");

			// Test di EuroWordnet
			// fare attenzione ai vari percorsi all'interno dei file di configurazione

			System.out.println("\n*****************************************");
			System.out.println("Test di EUROWORDNET ");
			System.out.println("******************************************\n");

			LinguisticResource lResEUROWORNDET = lingModel.getLinguisticResource("EuroWordnet");
			LinguisticInterface lIntEUROWORDNET = lResEUROWORNDET.loadLinguisticInterface();
			// LinguisticInterface lIntWORDNET = lResWORNDET.getLinguisticInterface();
			lIntEUROWORDNET.initialize();

			String termEuroWordnet = "gatto";
			Collection<SemanticIndex> resultsEuroWordnet = ((ConceptualizedLR) lIntEUROWORDNET)
					.getConcepts(new SearchWord(termEuroWordnet));
			System.out.println("\n\tSearched \"" + termEuroWordnet + "\" on EuroWordNet:");
			for (SemanticIndex conceptEuroWordnet : resultsEuroWordnet) {
				System.out.print("\t\t" + conceptEuroWordnet.getConceptRepresentation());
				// String gloss = ((LRWithGlosses)lResWORNDET).getConceptGloss(concept);
				String[] stringheEuroWornet = ((ConceptualizedLR) lIntEUROWORDNET)
						.getConceptLexicals(conceptEuroWordnet);
				System.out.print("\n\t\tSyn: ");
				for (String synWordnet : stringheEuroWornet) {
					System.out.print(synWordnet + " / ");
				}
				String glossWordnet = ((LRWithGlosses) lIntEUROWORDNET).getConceptGloss(conceptEuroWordnet);
				System.out.print("\n\t\tGloss: " + glossWordnet + "\n\n");

			}

			System.out.println("unloading LinguisticInterface");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
			lingModel.unloadLoadedInstances();
			System.out.println("LinguisticInterface unloaded");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");

		} catch (PMInitializationException e) {
			e.printStackTrace();
		} catch (LingModelReadException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (LinguisticInterfaceLoadException e) {
			e.printStackTrace();
		}

	}

}
