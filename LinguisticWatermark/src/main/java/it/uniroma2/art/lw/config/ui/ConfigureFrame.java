package it.uniroma2.art.lw.config.ui;

//import it.uniroma2.art.lw.LinguisticInterfacesFactory;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.objects.LIFactory;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

//TODO put it into a config.ui package
/**
 * @author Donato Griesi, Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati
 *         <turbati@info.uniorma2.it>
 */
public class ConfigureFrame extends javax.swing.JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5984860375519380907L;
	private JTabbedPane jTabbedPane;
	private JButton buttonClose;
	private HashMap<String, JPanel> resourcesPanelMap;
	private HashMap<PropertyInstanceValue, JTextField> textFieldMap;

	private ArrayList<JPanel> instancePanelsList;
	private ConfigureFrame thisFrame;

	private LingModel lingModel;

	// private boolean change = false;

	public static void main(String[] args) {
	}

	public JButton getSaveButton() {
		return buttonClose;
	}

	public ConfigureFrame() {
		this.thisFrame = this;
	}

	// public void initialize(Vector<String> interfacesIDs, final Vector<String>
	// vector) {
	public void initialize(final LingModel lModel) {

		this.lingModel = lModel;

		jTabbedPane = new JTabbedPane();
		resourcesPanelMap = new HashMap<String, JPanel>();
		textFieldMap = new HashMap<PropertyInstanceValue, JTextField>();

		JPanel bottomPanel = new JPanel();
		bottomPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black, 1),
				BorderFactory.createBevelBorder(BevelBorder.RAISED)));
		Border edge = BorderFactory.createRaisedBevelBorder();
		Dimension size = new Dimension(130, 20);
		buttonClose = new JButton("Close");
		buttonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		bottomPanel.add(buttonClose);

		buttonClose.setBorder(edge);
		buttonClose.setPreferredSize(size);

		JPanel panelGlobal = new JPanel();
		BoxLayout boxLayout = new BoxLayout(panelGlobal, BoxLayout.Y_AXIS);
		panelGlobal.setLayout(boxLayout);
		panelGlobal.add(jTabbedPane);
		panelGlobal.add(bottomPanel);
		this.getContentPane().add(panelGlobal);
	}

	private void setTabPanel() {
		Iterator<String> itertIntrfaceIDs = lingModel.getLinguisticInterfacesIDs().iterator();
		jTabbedPane.removeAll();
		while (itertIntrfaceIDs.hasNext()) {
			final String labelInterfaceId = itertIntrfaceIDs.next();
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			resourcesPanelMap.put(labelInterfaceId, panel);
			JScrollPane pane = new JScrollPane(panel);

			jTabbedPane.addTab(labelInterfaceId, pane);
		}
	}

	private void setProperties() throws PropertyNotFoundException {
		Iterator<String> it = lingModel.getLinguisticInterfacesIDs().iterator();
		instancePanelsList = new ArrayList<JPanel>();

		while (it.hasNext()) {
			GridBagLayout interfaceBag = new GridBagLayout();
			GridBagLayout instanceBag = new GridBagLayout();

			JPanel interfacePanel = new JPanel(interfaceBag);
			interfacePanel.setBorder(new TitledBorder(new EtchedBorder(), "InterfaceProperties"));
			final JPanel instancePanel = new JPanel(instanceBag);
			instancePanelsList.add(instancePanel);
			instancePanel.setBorder(new TitledBorder(new EtchedBorder(), "InstanceProperties"));

			GridBagConstraints limInterface = new GridBagConstraints();
			GridBagConstraints limInstance = new GridBagConstraints();

			final String interfaceId = it.next();
			JPanel panel = (JPanel) resourcesPanelMap.get(interfaceId);

			LIFactory liFact = lingModel.getLinguisticResourceType(interfaceId).getLIFactory();
			Collection<String> interfaceProperties = liFact.getInterfaceProperties();
			int interface_Y = 0;
			for (String propID : interfaceProperties) {
				JLabel label = new JLabel(propID + ":");
				limInterface.anchor = GridBagConstraints.NORTHWEST;
				limInterface.fill = GridBagConstraints.HORIZONTAL;
				limInterface.gridx = 0;
				limInterface.gridy = interface_Y;
				limInterface.weightx = 1;
				limInterface.weighty = 1;
				limInterface.gridwidth = 1;
				limInterface.gridheight = 1;
				interfaceBag.setConstraints(label, limInterface);
				interfacePanel.add(label);

				Component strut = Box.createHorizontalGlue();
				limInterface.gridx = 1;
				limInterface.gridy = interface_Y;
				limInterface.weightx = 2;
				interfaceBag.setConstraints(strut, limInterface);
				interfacePanel.add(strut);

				String propValue = liFact.getPropertyValue(propID);
				JTextField textField = new JTextField(propValue, 20);
				textField.setToolTipText(liFact.getPropertyDescription(propID));
				textField.setEditable(false);
				limInterface.gridx = 2;
				limInterface.gridy = interface_Y;
				limInterface.weightx = 2;
				interfaceBag.setConstraints(textField, limInterface);
				interfacePanel.add(textField);
				PropertyInstanceValue propInstVal = new PropertyInstanceValue(propID, null, propValue,
						interfaceId);
				textFieldMap.put(propInstVal, textField);

				interface_Y++;
			}
			JButton buttonEditInterface = new JButton("Edit Interface");
			buttonEditInterface.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					EditResourceFrame frame = new EditResourceFrame();
					frame.initialize(thisFrame, lingModel, null, interfaceId, false);
					frame.setVisible(true);
					enableFrame(false);
				}
			});
			limInterface.gridx = 0;
			limInterface.gridy = interface_Y;
			limInterface.weightx = 2;
			instanceBag.setConstraints(buttonEditInterface, limInterface);
			interfacePanel.add(buttonEditInterface);

			// INSTACES PART
			Iterator<String> it2 = lingModel.getInstancesIDs().iterator();

			final Vector<String> instanceElementForInterface = new Vector<String>();
			int instance_Y = 0;
			while (it2.hasNext()) {
				final String instanceElementId = it2.next();
				String interfaceIdOfInstance = lingModel.getLinguisticResource(instanceElementId)
						.getLinguisticInterfaceID();
				if (!interfaceId.equals(interfaceIdOfInstance))
					continue;
				instanceElementForInterface.add(instanceElementId);

				JLabel textFieldDescription = new JLabel("Instance Name:");
				textFieldDescription.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limInstance.anchor = GridBagConstraints.WEST;
				limInstance.fill = GridBagConstraints.NONE;
				limInstance.gridx = 0;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 1;
				limInstance.gridwidth = 1;
				limInstance.gridheight = 1;
				instanceBag.setConstraints(textFieldDescription, limInstance);
				instancePanel.add(textFieldDescription);

				JTextField instanceId = new JTextField(instanceElementId);
				instanceId.setEditable(false);
				PropertyInstanceValue propInstValId = new PropertyInstanceValue("Instance Name",
						instanceElementId, instanceElementId, null);
				textFieldMap.put(propInstValId, instanceId);

				instanceId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limInstance.anchor = GridBagConstraints.WEST;
				limInstance.fill = GridBagConstraints.HORIZONTAL;
				limInstance.gridx = 2;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 1;
				limInstance.gridwidth = 1;
				limInstance.gridheight = 1;
				instanceBag.setConstraints(instanceId, limInstance);
				instancePanel.add(instanceId);
				instance_Y++;

				Collection<String> instanceProperties = liFact.getInstanceProperties();
				for (String propId : instanceProperties) {
					JLabel label = new JLabel(propId + ":");
					limInstance.anchor = GridBagConstraints.NORTHWEST;
					limInstance.fill = GridBagConstraints.HORIZONTAL;
					limInstance.gridx = 0;
					limInstance.gridy = instance_Y;
					limInstance.weightx = 1;
					limInstance.weighty = 1;
					limInstance.gridwidth = 1;
					limInstance.gridheight = 1;
					instanceBag.setConstraints(label, limInstance);
					instancePanel.add(label);

					Component strut = Box.createHorizontalGlue();
					limInstance.gridx = 1;
					limInstance.gridy = instance_Y;
					limInstance.weightx = 1;
					instanceBag.setConstraints(strut, limInstance);
					instancePanel.add(strut);

					LinguisticResource lRes = lingModel.getLinguisticResource(instanceElementId);

					String propValue = lRes.getPropertyValue(propId);
					JTextField textField = new JTextField(propValue);
					textField.setEditable(false);
					textField.setToolTipText(lRes.getPropertyDescription(propId));
					limInstance.gridx = 2;
					limInstance.gridy = instance_Y;
					limInstance.weightx = 2;
					instanceBag.setConstraints(textField, limInstance);
					instancePanel.add(textField);

					if (textFieldMap.containsKey(propId)) {
						textFieldMap.remove(propId);
					}
					PropertyInstanceValue propInstVal = new PropertyInstanceValue(propId, instanceElementId,
							propValue, null);
					textFieldMap.put(propInstVal, textField);

					instance_Y++;

				}
				JButton buttonRemoveInstance = new JButton("Remove Instance");

				buttonRemoveInstance.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						lingModel.removeLinguisticResource(instanceElementId);
						try {
							LinguisticWatermarkManager.save();
							drawConfigurePanel();
						} catch (PropertyNotFoundException e) {
							e.printStackTrace();
						}
					}
				});

				limInstance.gridx = 0;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 2;
				instanceBag.setConstraints(buttonRemoveInstance, limInstance);
				instancePanel.add(buttonRemoveInstance);

				JButton buttonEditInstance = new JButton("Edit Instance");
				buttonEditInstance.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						EditResourceFrame frame = new EditResourceFrame();
						frame.initialize(thisFrame, lingModel, instanceElementId, interfaceId, true);
						frame.setVisible(true);
						// setEnabled(false);
						enableFrame(false);
					}
				});
				limInstance.gridx = 2;
				limInstance.gridy = instance_Y;
				limInstance.weightx = 2;
				instanceBag.setConstraints(buttonEditInstance, limInstance);
				instancePanel.add(buttonEditInstance);
				instance_Y++;
			}

			while (it2.hasNext()) {
				String instanceElementValues = it2.next();
				if (!instanceElementValues.equals(interfaceId))
					continue;
				instanceElementForInterface.add(instanceElementValues);
			}

			if (interfaceProperties.size() != 0) {
				panel.add(interfacePanel);
			}

			JPanel instancePanelContainer = new JPanel();
			instancePanelContainer.setLayout(new BoxLayout(instancePanelContainer, BoxLayout.Y_AXIS));
			instancePanelContainer.add(instancePanel);
			JPanel buttonPanel = new JPanel();
			JButton createButton = new JButton("Create Instance");
			buttonPanel.add(createButton);

			createButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					CreateResFrame frame = new CreateResFrame();
					frame.initialize(thisFrame, lingModel, interfaceId);
					frame.setVisible(true);
					setEnabled(false);
				}
			});

			instancePanelContainer.add(buttonPanel);
			panel.add(instancePanelContainer);
		}
		pack();
	}

	public void drawConfigurePanel() throws PropertyNotFoundException {
		setTabPanel();
		setProperties();
		setSize(500, 700);
	}

	public String newInstanceId(Vector<String> selectedInstanceIdsVector2) {
		UUID uuid = null;
		boolean bool;
		do {
			bool = false;
			uuid = UUID.randomUUID();
			for (String instanceId : selectedInstanceIdsVector2) {
				if (instanceId.equals(uuid.toString())) {
					bool = true;
				}
			}
		} while (bool);
		return uuid.toString();
	}

	public void enableFrame(boolean enable) {
		// setEnabled(true);
		setVisible(enable);
		try {
			drawConfigurePanel();
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
	}
}
