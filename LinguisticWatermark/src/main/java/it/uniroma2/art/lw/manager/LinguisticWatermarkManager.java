 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.manager;

import it.uniroma2.art.lw.config.ui.ConfigureFrame;
import it.uniroma2.art.lw.config.ui.LoaderFrame;
import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LingModelStoreException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LingModelFactory;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;

import java.awt.Frame;
import java.io.File;
import java.util.Collection;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */

public class LinguisticWatermarkManager {
	
	private static ConfigureFrame configureFrame = new ConfigureFrame();
	private static LoaderFrame loaderFrame= new LoaderFrame();
	
	private static LingModel lingModel;
	private static Collection <String>loadedRes;
	private static String selectedLingInt;
	
	
	static public void setLinguisticModel(File oSGiDir, String rootBundleDir, boolean subDirPresent, File configFile) throws PMInitializationException, LingModelReadException, LinguisticInterfaceLoadException {
		lingModel = LingModelFactory.initializeLingModel(oSGiDir, rootBundleDir, subDirPresent, configFile);
	}
	
	static public LingModel getLinguisticModel(){
		return lingModel; 
	}
	
	static public void initializeFrame(){
		configureFrame.initialize(lingModel);
		loaderFrame.initialize(lingModel);
	}
	
	static public ConfigureFrame getConfigureFrame(){
		return configureFrame;
	}
	
	static public Frame getLoaderFrame(){
		return loaderFrame;
	}

	
	static public void setLoadedResName(Collection<String> loadedResName){
		loadedRes = loadedResName;
		
	}
	
	static public Collection<String> getLoadedResName(){
		return loadedRes;
	} 
	
	static public LinguisticInterface getSelectedInterface() {
		//LinguisticInterface lInt;
		
		LinguisticResource lRes = lingModel.getLinguisticResource(selectedLingInt);
		if(lRes != null ){
			return lRes.getLinguisticInterface();
		}
		return null;
	}
	
	
	
	static public void setLoadedInterface(String instId){
		
		selectedLingInt = instId;
		
		//lingModel.clearSelectedInstances();
		
		
		
		/*
		try {
			lingModel.unloadLoadedInstances();
			lingModel.getLinguisticResource(instId).loadLinguisticInterface();
		} catch (LinguisticInterfaceLoadException e) {
			e.printStackTrace();
		}
		*/
		//lingModel.addSelectedInstance(instId);
	}
	

	public static void save() {
		try {
			lingModel.save(null);
		} catch (LingModelStoreException e) {
			e.printStackTrace();
		}
		
	}
}
