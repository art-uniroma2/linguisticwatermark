/*
 * Created on 15-giu-2005
 *
 */
package it.uniroma2.art.lw.model.objects;

import java.util.Collection;

/**
 * @author Donato
 */
public interface ConceptPrefixSearch {
	abstract public Collection<SemanticIndex> getConceptsByPrefixStrategy(String lemma);
}
