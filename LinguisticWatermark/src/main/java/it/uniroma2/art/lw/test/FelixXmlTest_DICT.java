package it.uniroma2.art.lw.test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LingModelFactory;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.objects.BilingualLinguisticInterface;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.TaxonomicalLR;

public class FelixXmlTest_DICT {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		File oSGiDir = new File("oSGiDir");
		String rootDirBundle = "rootBundleDir";
		File configFile = new File(
				"config/LWConfig_DICT.xml");

		LingModel lingModel = null;

		try {
			lingModel = LingModelFactory.initializeLingModel(oSGiDir, rootDirBundle, false, configFile, false);

			System.out.println("Felix è stato avviato e file XMl è stato letto");

			// Test di Dict eng-ita
			lingModel.unloadLoadedInstances();
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");

			System.out.println("\n*****************************************");
			System.out.println("Test di Dict eng-ita");
			System.out.println("******************************************\n");
			LinguisticResource lResEngIta = lingModel.getLinguisticResource("eng-ita");
			LinguisticInterface lIntDictEngIta = lResEngIta.loadLinguisticInterface();
			// LinguisticInterface lIntDictEngIta = lResEngIta.getLinguisticInterface();
			lIntDictEngIta.initialize();

			// System.out.println(lIntEngIta.isConceptualized());
			// System.out.println(lIntEngIta.hasGlosses());
			String termDict = "window";
			Collection<SemanticIndex> resultsDict = ((ConceptualizedLR) lIntDictEngIta)
					.getConcepts(new SearchWord(termDict));
			System.out.println("\n\tSearched \"" + termDict + "\" on Dict eng-ita:");
			for (SemanticIndex conceptDict : resultsDict) {
				System.out.print("\t\t" + conceptDict.getConceptRepresentation());
				String[] stringheDict = ((ConceptualizedLR) lIntDictEngIta).getConceptLexicals(conceptDict);
				System.out.print("\n\t\tSyn: ");
				for (String synDict : stringheDict) {
					System.out.print(synDict + " / ");
				}
				System.out.print("\n\n");
			}
			
			
			
			termDict = "finestra";
			boolean isBidirectional = ((BilingualLinguisticInterface) lIntDictEngIta).isBidirectional();
			System.out.println("isBidirectional = "+isBidirectional);
			
			
			System.out.println("unloading LinguisticInterface");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
			lingModel.unloadLoadedInstances();
			System.out.println("LinguisticInterface unloaded");
			System.out.println("Ci sono " + lingModel.getNumLoadedLInt() + " LinguisticResource caricate");
		

		} catch (PMInitializationException e) {
			e.printStackTrace();
		} catch (LingModelReadException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (LinguisticInterfaceLoadException e) {
			e.printStackTrace();
		}
		
		System.exit(0);

	}

}
