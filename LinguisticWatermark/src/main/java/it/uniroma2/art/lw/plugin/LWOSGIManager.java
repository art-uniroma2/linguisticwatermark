/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is Linguistic Watermark.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
 * All Rights Reserved.
 *
 * Linguistic Watermark was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
 * Current information about Linguistic Watermark can be obtained at 
 * http://art.uniroma2.it/software/LinguisticWatermark/
 *
 */

package it.uniroma2.art.lw.plugin;

import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.model.objects.LIFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Andrea Turbati, Armando Stellato
 */

public class LWOSGIManager extends ARTOSGiManager<LWOSGIExtension> {

	/**
	 * the PluginManager class is typically used as a singleton. This variable contains the single instance
	 * which is accessed to manage OSGi extensions.
	 */
	private static LWOSGIManager manager = null;

	protected static Log logger = LogFactory.getLog(LWOSGIManager.class);

	private static final String resourceRelPath = "lw_resource";
	private static final String enrichmentRelPath = "lw_enrichent";
	
	private LWOSGIManager(File felixDir) throws PMInitializationException {
		super(felixDir);
	}

	/**
	 * Method to inialize the OSGi framwork using felix implementation
	 * @param oSGiDir
	 * @return
	 * @throws PMInitializationException
	 */
	public static LWOSGIManager initialize(File oSGiDir) throws PMInitializationException {
		manager = new LWOSGIManager(oSGiDir);
		return manager;
	}

	public static LWOSGIManager getManager() {
		return manager;
	}

	public void loadEnrichMethods(String rootDir, boolean subDirPresent) {

		bundleToBeStarted.clear();
		if (subDirPresent) {
			File[] subDirs = (new File(rootDir)).listFiles();
			if(subDirs == null)
				return;
			for(File subDir : subDirs){
				File []resourceDirs = (new File(subDir, enrichmentRelPath)).listFiles();
				if(resourceDirs == null)					
					return;
				for(File resourceDir : resourceDirs){
					findAndInstallPlugin(resourceDir);
				}
			}
		}
		else{
			File[] resourceDirs = (new File(rootDir, enrichmentRelPath).listFiles());
			if (resourceDirs == null)
				return;
			for(File resourceDir : resourceDirs){
				findAndInstallPlugin(resourceDir);
			}
		}
		
		// Faccio la start dei bundle appena trovati
		startAllBundle();


	}

	/**
	 * Funzione che carica in Felix i bundle relativi alle implementazioni dell'interfaccia di Linguistic
	 * Watermark
	 * 
	 * @param dirList
	 *            lista di directory in cui cercare i bundle
	 */
	public void loadFactoryLW(String rootDir, boolean subDirPresent) {
		//System.out.println("\tinizio loadFactoryLW"); // da cancellare
		bundleToBeStarted.clear();
		if (subDirPresent) {
			//System.out.println("\tsubDirPresent = "+subDirPresent); // da cancellare
			File[] subDirs = (new File(rootDir)).listFiles();
			if(subDirs == null)
				return;
			for(File subDir : subDirs){
				File []resourceDirs = (new File(subDir, resourceRelPath)).listFiles();
				if(resourceDirs == null)					
					continue;
				for(File resourceDir : resourceDirs){
					//System.out.println("\tresourceDir.getAbsolutePath() = "+resourceDir.getAbsolutePath()); // da cancellare
					findAndInstallPlugin(resourceDir);
				}
			}
		}
		else{
			System.out.println("rootDir = "+rootDir);
			//System.out.println("\tsubDirPresent = "+subDirPresent); // da cancellare
			File[] resourceDirs = (new File(rootDir, resourceRelPath).listFiles());
			if (resourceDirs == null)
				return;
			for(File resourceDir : resourceDirs){
				System.out.println("resourceDir = "+resourceDir.getAbsolutePath());
				findAndInstallPlugin(resourceDir);
			}
		}
		
		startAllBundle();


	}

	/**
	 * generates a list of the loaded factories for Linguistic Interfaces dynamically loaded through Felix
	 */
	public HashMap<String, LIFactory> getLinguisticInterfaceFactoryList() {

		HashMap<String, LIFactory> linguisticInterfacesFactoryMap = new HashMap<String, LIFactory>();

		ArrayList<LIFactory> liFatories = getExtensionsForType(LIFactory.class);

		for (LIFactory liFactory : liFatories) {
			linguisticInterfacesFactoryMap.put(liFactory.getId(), liFactory);
		}

		return linguisticInterfacesFactoryMap;
	}

	/**
	 * generates a list of the loaded enrichment methods dynamically loaded through Felix
	 */
	public HashMap<String, LIFactory> getEnrichmentMethodsList() {

		HashMap<String, LIFactory> enrichmentMethodMap = new HashMap<String, LIFactory>();

		// TODO

		return enrichmentMethodMap;
	}

}
