 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * SemanticTurkey was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about SemanticTurkey can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.model.store.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.w3c.dom.Element;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


/**
 * @author Donato Griesi, Armando Stellato, Andrea Turbati
 *
 */
public class XMLHelp {

  

  /**
 * @param xml
 * @param indent
 * @deprecated
 * @return
 */
public static String XML2String (Element xml, boolean indent) {
    if (xml == null) {
      return null;
    }
    else {
      ByteArrayOutputStream stringOut = new ByteArrayOutputStream();
      try {
        OutputFormat format = new OutputFormat(xml.getOwnerDocument()); //Serialize DOM
        format.setOmitXMLDeclaration(true);
        format.setIndenting(indent);
        format.setEncoding("UTF-8");
        XMLSerializer serial = new XMLSerializer(stringOut, format);
        serial.asDOMSerializer(); // As a DOM Serializer
        serial.serialize(xml);
      }
      catch (IOException e) {}
      return stringOut.toString();
    }
  }


    /**
     * fast create and add-as-child method for XML
     * @param parent
     * @param nm
     * @return
     */
    public static Element addNewChildElement(Element parent, String nm) {
        Element oNode = parent.getOwnerDocument().createElement(nm);
        parent.appendChild(oNode);
        return oNode;
    }

    /**
     * fast create element with value and add-as-child method for XML
     * 
     * @param parent
     * @param nm
     * @param val
     * @return
     */
    public static Element addNewChildElementWithValue(Element parent, String nm, String val) {
        Element oNode = null;
        oNode = parent.getOwnerDocument().createElement(nm);
        oNode.appendChild(parent.getOwnerDocument().createTextNode(val));
        parent.appendChild(oNode);
        return oNode;
    }


}