package it.uniroma2.art.lw.config.ui;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LinguisticResource;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

//TODO put it into a config.ui package
/**
 * @author Donato Griesi, Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class LoaderFrame extends javax.swing.JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8323478633832310529L;
	private JButton buttonOK = null;
	//private JButton buttonClose = null;
	//private ButtonGroup bg = null;
	private LingModel lingModel;
	//private int numMaxLoaded;
	private int numResLoaded;
	private Box box;
	private ArrayList<String> loadedLingRes;
	private HashMap<String, doubleButton> mapButton;
	
	
	public LoaderFrame(){
		loadedLingRes = new ArrayList<String>();
		mapButton = new HashMap<String, doubleButton>();
	}
	
	public void initialize(LingModel lingModel) {
		//this.numMaxLoaded = maxLoaded;
		numResLoaded = lingModel.getNumLoadedLInt();
		JPanel panel = new JPanel();		
		BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
		panel.setLayout(boxLayout);
		this.lingModel = lingModel;
		
		box = Box.createVerticalBox();		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black, 1),
		BorderFactory.createBevelBorder(BevelBorder.RAISED)));
		Border edge = BorderFactory.createRaisedBevelBorder();
						
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		topPanel.setBorder(new TitledBorder(new EtchedBorder(), "Choose a resource to load"));
		topPanel.add(box, BorderLayout.CENTER);							
		JScrollPane commonPane = new JScrollPane(topPanel);		
		
		
		//drawSlectionPanel();
		
		//add(commonPane);
		
		panel.add(commonPane);
		
		
		Dimension size = new Dimension(130, 20);
		buttonOK = new JButton("OK");
		buttonOK.setBorder(edge);		
		buttonOK.setPreferredSize(size);
				
		bottomPanel.add(buttonOK);
		
		panel.add(bottomPanel);
		
		add(panel);
		
		pack();
		//setSize(400, 200);				
	}
	
	public JButton getOKButton() {
		return buttonOK;
	}
	
	public Vector<Selection> getSelection() {
        Vector<Selection> vect = new Vector<Selection>();
        for(String idRes : loadedLingRes){
        	vect.add(new Selection( lingModel.getLinguisticResource(idRes).getLinguisticInterfaceID(), idRes));
        }
        return vect;
        
    }
	
	public void drawSelectionPanel(){
		Collection<LinguisticResource> lResCollection = lingModel.getLinguisticResources();
		loadedLingRes.clear();
		box.removeAll();
		int numRes = lResCollection.size();
		if(numRes > 10)
			numRes = 10;
		setSize(400, numRes*60);
		for (final LinguisticResource lRes :  lResCollection) {
		    if(lRes.getLinguisticInterfaceFactory() != null) {
		    	JPanel panelTextAndButton = new JPanel();
		    	panelTextAndButton.setSize(360, 30);
				JPanel panelText = new JPanel();
				//panelText.setSize(160, 25);
				panelText.setPreferredSize(new Dimension(250,30));
				panelText.add(new JLabel(lRes.getId()));
				JPanel panelButton = new JPanel();
				//panelButton.setSize(100, 30);
				panelButton.setPreferredSize(new Dimension(95, 30));
				JButton buttonLoad = new JButton("Load");
				buttonLoad.setPreferredSize(new Dimension(60,20));
				buttonLoad.setBorder(BorderFactory.createLineBorder(Color.red));
				buttonLoad.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						//Load the selected LinguisticInterface
						try {
							lRes.loadLinguisticInterface();
							loadedLingRes.add(lRes.getId());
							if(numResLoaded++ == 0) {
								buttonOK.setEnabled(true);
							}
						} catch (LinguisticInterfaceLoadException ex) {
							lRes.unloadLinguisticInterface();
							// show a window displaying the message that the resource could not me loaded
							Frame frame = JOptionPane.getRootFrame();
							String errorMsg = "The Linguistic Resource "+lRes.getId()+" of "+lRes.getLinguisticInterfaceID()+" could not be loaded";
							JOptionPane.showMessageDialog(frame, errorMsg, "LinguisticResource Initialization Error!", JOptionPane.ERROR_MESSAGE);
						}
						
						setEnableButtons();
					}
					
				});
				JButton buttonUnload = new JButton("Unload");
				buttonUnload.setPreferredSize(new Dimension(60,20));
				buttonUnload.setBorder(BorderFactory.createLineBorder(Color.blue));
				buttonUnload.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						loadedLingRes.remove(lRes.getId());
						if(--numResLoaded == 0) {
							
							buttonOK.setEnabled(false);
						}
						//Unload the selected LinguisticInterface
						lRes.unloadLinguisticInterface();
						setEnableButtons();
					}
					
				});
				mapButton.put(lRes.getId(), new doubleButton(buttonLoad, buttonUnload));
				
				panelTextAndButton.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.black));
				
				panelTextAndButton.add(panelText);
				panelButton.add(buttonLoad, JPanel.CENTER_ALIGNMENT);
				panelButton.add(buttonUnload,  JPanel.CENTER_ALIGNMENT);
				panelTextAndButton.add(panelButton);
				
				box.add(panelTextAndButton);
				
				if(lRes.getLinguisticInterface() != null){
					loadedLingRes.add(lRes.getId());
					setEnableButton(buttonLoad, false);
					setEnableButton(buttonUnload,true);
				}
				else {
					setEnableButton(buttonLoad, true);
					setEnableButton(buttonUnload, false);
				}
			}
		}	
	}
	
	private void setEnableButton(JButton button, boolean enable){
		button.setVisible(enable);
	}
	
	private void setEnableButtons() {
		for(LinguisticResource lRes : lingModel.getLinguisticResources()){
			if(lRes.getLinguisticInterfaceFactory() != null){
				String idRes = lRes.getId();
				if(loadedLingRes.contains(idRes)){
					mapButton.get(idRes).setLoadButtonEnable(false);
					mapButton.get(idRes).setUnloadButtonEnable(true);
				}
				else { 
					mapButton.get(idRes).setLoadButtonEnable(true);
					mapButton.get(idRes).setUnloadButtonEnable(false);
				}
			}
		}
		
	}
	
	private class doubleButton{
		private JButton loadButton;
		private JButton unloadButton;
		
		public doubleButton(JButton loadButton, JButton unloadButton){
			this.loadButton = loadButton;
			this.unloadButton = unloadButton;
		}
		
		public void setLoadButtonEnable(boolean bool){
			loadButton.setVisible(bool);
		}
		
		public void setUnloadButtonEnable(boolean bool){
			unloadButton.setVisible(bool);
		}
	}
}
