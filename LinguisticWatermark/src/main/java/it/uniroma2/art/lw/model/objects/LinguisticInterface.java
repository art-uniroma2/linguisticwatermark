 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is LinguisticWatermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * LinguisticWatermark was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about LinguisticWatermark can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

package it.uniroma2.art.lw.model.objects;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;

import java.util.Collection;


/**
 * this is the main class which must be implemented to create a wrapper for a given (category of) linguistic resource
 * @author  Armando Stellato <stellato@info.uniroma2.it>
 */
public abstract class LinguisticInterface {
        
        
  /**
     * the natural language in which the resource is expressed, if the resource is a bilingual dictionary
     * it represents the target language of the translation 
     */
    protected String language;
    /**
     * a sort of very short ID used to identify the resource
     */
    protected String shortID;

    /**
     * this method initializes the resource, loading its database and specifying some of its properties (language, shortID etc...)
     */
    abstract public void initialize()throws LinguisticInterfaceLoadException;
    
    
    /**
     * @param Relation a semantic relation between senses of linguistic expressions inside the linguistic resource
     * @param c a sense related to one or more linguistic expressions
     * @return a set of results, depending on the nature of the linguistic resource (usually concepts). 
     * @throws LinguisticResourceAccessException 
     * @throws Exception
     */
    abstract public Collection<SemanticIndex> exploreSemanticRelation(SemanticIndex c, SemanticRelation Relation) throws LinguisticResourceAccessException;
    /*
    {
        try {
            return (Collection<SemanticIndex>)(this.getClass().getMethod("exploreSemanticRelation_"+Relation.getName(), it.uniroma2.art.lw.model.objects.SemanticIndex.class).invoke(this,c));
        } catch (IllegalArgumentException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (SecurityException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (IllegalAccessException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (InvocationTargetException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (NoSuchMethodException e) {
            throw new LinguisticResourceAccessException(e);
        }
    }
	*/
    
 
    /**
     * @param word a word
     * @param c a semantic index related to one or more linguistic expressions
     * @param Relation a lexical relation between pairs of <word,senses> of linguistic expressions inside the linguistic resource
     * @return a set of <word,sense> pairs
     * @throws LinguisticResourceAccessException
     */
    abstract public Collection<SemanticIndex> exploreLexicalRelation(String word, SemanticIndex c, LexicalRelation Relation) throws LinguisticResourceAccessException;   
    /*
    {
    	try {
            return (Collection<SemanticIndex>)(this.getClass().getMethod("exploreLexicalRelation_"+Relation.getName(), it.uniroma2.art.lw.model.objects.SemanticIndex.class, String.class).invoke(this,c,word));
        } catch (IllegalArgumentException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (SecurityException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (IllegalAccessException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (InvocationTargetException e) {
            throw new LinguisticResourceAccessException(e);
        } catch (NoSuchMethodException e) {
            throw new LinguisticResourceAccessException(e);
        }
    }
    */
    
    /**
     * @param SearchMethod a kind of search to be applied upon a given search key
     * @param words a word or linguistic expression used as a key for searching the linguistic resource 
     * @return a set of strings representing lemmas returned by the search
     * @throws Exception
     */
    public Collection<SearchWord> exploreSearchStrategy(String word, SearchStrategy searchMethod, SearchFilter ... searchFilters)
    {
        return getSearchWords(word, searchMethod.getName(), searchFilters);
    }    
    
    protected abstract Collection<SearchWord> getSearchWords(String word, String methodName, SearchFilter ... searchFilters);
    

    /**
     * @return the list of semantic relations which are available for the wrapped linguistic resource
     */
    public abstract Collection<SemanticRelation> getSemanticRelations();

   
	/**
	 * @return the list of lexical relations which are available for the wrapped linguistic resource
	 */
	public abstract Collection<LexicalRelation> getLexicalRelations();

	/**
     * @return the list of search strategies which are available from the linguistic interface
     */
    public abstract Collection<SearchStrategy> getSearchStrategies();
	
    /**
     * @return the list of search filters which are available from the linguistic interface
     */
    public abstract Collection<SearchFilter> getSearchFilters();
    
    
    public abstract SemanticRelation getSemanticRelation(String semRelation);
    
    public abstract LexicalRelation getLexicalRelation(String lexRelation);
    
    public abstract SearchStrategy getSearchStrategy(String searchStrategy);
    
    public abstract SearchFilter getSearchFilter(String searchFilter);
    
    
    /**
     * @return the value assigned to the <code>language</code> field of this interface   
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @return the value assigned to the <code>shortID</code> field of this interface
     */
    public String getResourceShortId() {
        return shortID;
    }

	
	/**
	 * @return true if the interface to the linguistic resource implements the <code>ConceptualizedLR</code> interface
	 */
	public boolean isConceptualized() {
	    return this instanceof ConceptualizedLR;
	}
	
	public boolean hasConceptPrefixSearch() {
	    return this instanceof ConceptPrefixSearch;
	}
	
	/**
	 * @return true if the interface to the linguistic resource implements the <code>TaxonomicalLR</code> interface
	 */
	public boolean isTaxonomical() {
	    return this instanceof TaxonomicalLR;
	}
	
	/**
	 * @return true if the interface to the linguistic resource implements the <code>LRWithGlosses</code> interface
	 */
	public boolean hasGlosses() {
	    return this instanceof LRWithGlosses;
	}
	
	/**
	 * @return true if the interface to the linguistic resource implements the <code>FlatLR</code> interface
	 */
	public boolean isFlat() {
	    return this instanceof FlatLR;
	}
	/**
	 * @return true if the interface to the linguistic resource is a subclass of <code>BilingualLinguisticInterface</code>
	 */
	public boolean isBilingual() {
	    return this instanceof BilingualLinguisticInterface;
	}

	/**
	 * @return true if this interface allows for switching between exact and non exact match when searching the resource for given word(s)
	 *         i.e. if the interface to the linguistic resource implements the <code>MatchCaseToggling</code> interface
	 */
	public boolean isSelectableOperationMatchCaseToggling() {
	    return this instanceof MatchCaseToggling;
	}	
	
	/**
	 * @return true if this interface allows for switching between case sensitive and case insensitive match when searching the resource for given word(s)
	 *         i.e. if the interface to the linguistic resource implements the <code>WholeWordToggling</code> interface 
	 */
	public boolean isSelectableOperationWholeWordToggling() {
	    return this instanceof WholeWordToggling;
	}
	
	
	
	
}
