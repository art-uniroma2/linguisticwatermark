/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is Linguistic Watermark.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
 * All Rights Reserved.
 *
 * Linguistic Watermark was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
 * Current information about Linguistic Watermark can be obtained at 
 * http://art.uniroma2.it/software/LinguisticWatermark/
 *
 */

package it.uniroma2.art.lw.startup;

import it.uniroma2.art.lw.exceptions.ReplacePlaceholderException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

public class PlaceholderReplacer {
	private Map<String, String> placeholderValueMap;
	private Map<String, String> placeholderIdNameMap;

	private String configInputFilePath;
	private String configOutputFilePath;

	private final String PLACEHOLDERS = "placeholders";
	private final String PLACEHOLDER = "placeholder";
	private final String PLCHID = "plchId";
	//private final String PLCHNAME = "plchVar";
	private final String PLCHNAME = "plchName";
	private final String PROPERTY = "property";
	private final String PLCHVALUETYPE = "plchValueType";
	private final String VALUE = "value";

	private final String FILE = "file";
	private final String FILEDELPROP = "fileDelProp";
	private final String DIR = "dir";
	private final String STRING = "string";

	public PlaceholderReplacer(Map<String, String> placeholderValueMap, String configFilePath) {
		initialize(placeholderValueMap, configFilePath, configFilePath);
	}

	public PlaceholderReplacer(Map<String, String> placeholderValueMap, String configInputFilePath,
			String configOutputFilePath) {
		initialize(placeholderValueMap, configInputFilePath, configOutputFilePath);
	}

	private void initialize(Map<String, String> placeholderValueMap, String configInputFilePath,
			String configOutputFilePath) {
		this.placeholderValueMap = placeholderValueMap;
		this.configInputFilePath = configInputFilePath;
		this.configOutputFilePath = configOutputFilePath;
		placeholderIdNameMap = new HashMap<String, String>();
	}

	public boolean replacePlaceholders() throws ReplacePlaceholderException {
		DOMParser parser = new DOMParser();
		try {
			parser.parse(configInputFilePath);
			Document document = parser.getDocument();
			if (document.getElementsByTagName(PLACEHOLDERS).getLength() == 0)
				return false;

			NodeList placeholderNodeList = document.getElementsByTagName(PLACEHOLDER);
			for (int i = 0; i < placeholderNodeList.getLength(); ++i) {
				// for every placeholder in the placeholders take the two attribute: plchId e plchVar and
				// create the map
				Element placeholderElement = (Element) placeholderNodeList.item(i);
				String id = placeholderElement.getAttribute(PLCHID);
				String name = placeholderElement.getAttribute(PLCHNAME);
				placeholderIdNameMap.put(id, name);
			}

			// take all the element property and check in the value attribute contains a placeholder, in that
			// case replace it.
			// It is possible that there can be more than one placeholder, so check for all of them
			NodeList propertyNodeList = document.getElementsByTagName(PROPERTY);
			for (int i = 0; i < propertyNodeList.getLength(); ++i) {
				Element propertyElement = (Element) propertyNodeList.item(i);
				String propValue = propertyElement.getAttribute(VALUE);
				if (propertyElement.hasAttribute(PLCHVALUETYPE)) {
					String plchValueType = propertyElement.getAttribute(PLCHVALUETYPE);
					for (String placeholderId : placeholderValueMap.keySet()) {
						String placeholderValue = placeholderValueMap.get(placeholderId);
						String placeholderName = placeholderIdNameMap.get(placeholderId);
						if (placeholderName == null)
							continue;
						//propValue = propValue.replaceAll(placeholderName, placeholderValue);
						propValue = propValue.replace(placeholderName, placeholderValue);
					}

					if (plchValueType.compareTo(FILE) == 0) {
						// open the file with that path and replace all the placeholder inside him
						replacePlaceholderInFile(propValue);
					}
					else if (plchValueType.compareTo(FILEDELPROP) == 0 ){
						// open the file with that path and replace all the placeholder inside him
						replacePlaceholderInFile(propValue);
						//remove the property form the DOM
						propertyElement.getParentNode().removeChild(propertyElement);
					}

					propertyElement.removeAttribute(PLCHVALUETYPE);
				}
				propertyElement.setAttribute(VALUE, propValue);

			}
			// remove the placeholders part from the DOM
			Node placehodersNode = document.getElementsByTagName(PLACEHOLDERS).item(0);
			placehodersNode.getParentNode().removeChild(placehodersNode);

			// save the configFile to the configOutputFilePath
			saveConfigFile(document);

		} catch (SAXException e) {
			throw new ReplacePlaceholderException(e);
		} catch (IOException e) {
			throw new ReplacePlaceholderException(e);
		}

		return true;
	}

	private void replacePlaceholderInFile(String filePath) throws ReplacePlaceholderException {
		File fileInput = new File(filePath);
		FileInputStream fis = null;
		StringBuilder contents = null;
		String contentOfTheFile;
		try {
			// read the input file
			fis = new FileInputStream(fileInput);
			InputStreamReader inputStreamReader = new InputStreamReader(fis, "UTF-8");
			BufferedReader br = new BufferedReader(inputStreamReader);

			String line;
			contents = new StringBuilder();

			while ((line = br.readLine()) != null) {
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}

			// dispose all the resources after using them.
			fis.close();
			inputStreamReader.close();
			br.close();

			// replace all the placeholders in the file
			contentOfTheFile = contents.toString();
			for (String placeholderId : placeholderValueMap.keySet()) {
				String placeholderValue = placeholderValueMap.get(placeholderId);
				String placeholderName = placeholderIdNameMap.get(placeholderId);
				if (placeholderName == null)
					continue;
				boolean somtehingWasReplaced = true;
				String contentOfTheFileReplaced;
				//TODO there is a problem with the raplaceAll, so this is a temporary solution
				while(somtehingWasReplaced){
					contentOfTheFileReplaced = contentOfTheFile.replace(placeholderName, placeholderValue);
					if(contentOfTheFile.compareTo(contentOfTheFileReplaced) == 0)
						somtehingWasReplaced = false;
					contentOfTheFile = contentOfTheFileReplaced;
				}
			}

			// delete the file
			// fileInput.delete();

			// save the file
			File fileOutput = new File(filePath);
			//FileWriter fstream = new FileWriter(fileOutput);
			BufferedWriter out = 
				new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOutput),"UTF-8"));
			//BufferedWriter out = new BufferedWriter(fstream);
			out.write(contentOfTheFile);

			// dispose all the resources after using them.
			out.close();

		} catch (FileNotFoundException e) {
			throw new ReplacePlaceholderException(e);
		} catch (IOException e) {
			throw new ReplacePlaceholderException(e);
		}
	}

	private void saveConfigFile(Document document) throws ReplacePlaceholderException {
		File configOutputFile = new File(configOutputFilePath);
		if (configOutputFile.exists())
			configOutputFile.delete();

		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new FileWriter(configOutputFile));
			transformer.transform(source, result);

		} catch (IOException e) {
			throw new ReplacePlaceholderException(e);
		} catch (TransformerConfigurationException e) {
			throw new ReplacePlaceholderException(e);
		} catch (TransformerException e) {
			throw new ReplacePlaceholderException(e);
		}
	}
}
