 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */

package it.uniroma2.art.lw.model.store.xml;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LingModelStoreException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManagerWithNoAWT;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.LinguisticResourceType;
import it.uniroma2.art.lw.model.store.LingModelIO;
import it.uniroma2.art.lw.model.store.LingModelIOParameters;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.dom.DocumentImpl;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class LingModelXMLIO implements LingModelIO {

    protected static Log logger = LogFactory.getLog(LingModelXMLIO.class);
    
    File backendXMLFile;
    
    public LingModelXMLIO(File backendXMLFile) {
        this.backendXMLFile = backendXMLFile;
    }
    
    public void setFile(File file){
    	backendXMLFile = file;
    }
    
    public void populateLingModel(LingModel lm) throws LingModelReadException {
    	populateLingModel(lm, true);
    }
    
    /* (non-Javadoc)
     * @see it.uniroma2.art.lw.model.store.LingModelIO#populateLingModel(it.uniroma2.art.lw.model.LingModel)
     */
    public void populateLingModel(LingModel lm, boolean useAWT) throws LingModelReadException { // LinguisticInterfaceLoadException
        
    	logger.info("parsing the file: "+backendXMLFile.getAbsolutePath());
    	
        Document doc;
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();    	
	    DocumentBuilder db;
        try {
            db = dbf.newDocumentBuilder();
            doc = db.parse(backendXMLFile);
        } catch (ParserConfigurationException e) {
            throw new LingModelReadException(e);            
        } catch (SAXException e) {
	        throw new LingModelReadException(e);
        } catch (IOException e) {
            throw new LingModelReadException(e);
        }
    	
    	doc.getDocumentElement().normalize();
    	
    	NodeList nodeLW = doc.getElementsByTagName("linguistic_watermark");
    	
    	////////////////////////
        // interfaces section //
        ////////////////////////

    	NodeList nodesInterfacesList = doc.getElementsByTagName("interfaces").item(0).getChildNodes();
    	for(int i=0; i<nodesInterfacesList.getLength(); ++i) {
    	    Node interfaceNode = nodesInterfacesList.item(i);
    		if(!(interfaceNode instanceof Element))
    			continue;
    		Element interfaceElement = (Element)interfaceNode;
    	    String interfaceId = interfaceElement.getAttribute("id");
    	    //LIFactory lingIntFact = lm.getLinguisticInterfaceFactory(interfaceId);
    	    LinguisticResourceType lResType = lm.getLinguisticResourceType(interfaceId);
    	    if(lResType==null){ // this condition should be always true, unless there are two lResType with the same id in the file
    	    	lResType = new LinguisticResourceType(interfaceId);
    	    	lm.addLinguisticResourceType(lResType);
    	    }
    	    
	    	//Collection<String> properties = lingIntFact.getInterfaceProperties();
            //properties.remove(o);
            
            //Class lingInterfaceClass = lingIntFact.getLinguisticInterfaceClass();
            
            NodeList propertyNodes = interfaceElement.getElementsByTagName("property");
            try {
                for(int k=0; k<propertyNodes.getLength(); ++k) {
                    Element propertyElement = (Element)propertyNodes.item(k);
                    String propId = propertyElement.getAttribute("id");
                    String propValue = propertyElement.getAttribute("value");
                    lResType.setProperty(propId, propValue);                    
                }    
            } catch (PropertyNotFoundException e) {
                throw new LingModelReadException(e);
            }    
    	}
    	
    	
    	
    	
    	////////////////////////
        // instance section   //
        ////////////////////////
    	
    	NodeList nodesInstancesList = doc.getElementsByTagName("instances_specification").item(0).getChildNodes();
    	
    	for(int i=0; i<nodesInstancesList.getLength(); ++i){
    		Node instanceNode = nodesInstancesList.item(i);
    		if(!(instanceNode instanceof Element))
    			continue;
    		Element instanceElement = (Element)instanceNode;
    		
    		String instanceId = instanceElement.getAttribute("id");
    		LinguisticResource lRes = new LinguisticResource(instanceId);
    		
    		String interfaceId = instanceElement.getAttribute("interface");
    		lRes.setLinguisticInterfaceID(interfaceId);
    		LinguisticResourceType lResType = lm.getLinguisticResourceType(interfaceId);
    		if(lResType != null && lResType.getLIFactory() != null){
    			lRes.setLinguisticInterfaceFactory( lResType.getLIFactory() );  
    		}
    		
    		NodeList propertyNodes = instanceElement.getElementsByTagName("property");
    		for(int k=0; k<propertyNodes.getLength(); ++k){
    			Element propertyElement = (Element)propertyNodes.item(k);
    			lRes.setProperty(propertyElement.getAttribute("id"), propertyElement.getAttribute("value"));    			
    		}
    		lm.addLinguisticResourse(lRes);
    	}
    	
    	
    	////////////////////////
    	// loaded instances   //
    	////////////////////////
    	
    	NodeList nodeSelectedInstances = doc.getElementsByTagName("selected_instances").item(0).getChildNodes();
    	boolean selectedInstance = false;
    	Collection<String> loadedRes = new Vector<String>();
    	//lm.clearSelectedInstances();
    	for(int i=0; i<nodeSelectedInstances.getLength(); ++i) {
    		Node selectedInstanceNode = nodeSelectedInstances.item(i);
    		if(!(selectedInstanceNode instanceof Element))
    			continue;
    		Element selectedInstanceElement = (Element) selectedInstanceNode;
    		
    		String id = selectedInstanceElement.getAttribute("id");
    		LinguisticResource lRes = lm.getLinguisticResource(id);
    		if(lRes.isLoadable() == false)
    			continue;
    		try{
    			lRes.loadLinguisticInterface();
    			loadedRes.add(id);
	    		if(selectedInstance == false){
	    			if(useAWT) {
	    				LinguisticWatermarkManager.setLoadedInterface(id);
	    			} else{
	    				LinguisticWatermarkManagerWithNoAWT.setLoadedInterface(id);
	    			}
	    			selectedInstance = true;
	    		}
	    		//lm.addSelectedInstances(id);
	    		logger.debug("SelectedInstances: "+id);
    		} catch (LinguisticInterfaceLoadException e){
    			lRes.unloadLinguisticInterface();
    		}
    	}
    	if(useAWT) {
    		LinguisticWatermarkManager.setLoadedResName(loadedRes);
    	} else{
			LinguisticWatermarkManagerWithNoAWT.setLoadedResName(loadedRes);
		}
    	//LinguisticWatermarkManager.save();

    }

    /* (non-Javadoc)
     * @see it.uniroma2.art.lw.model.store.LingModelIO#storeLingModel(it.uniroma2.art.lw.model.LingModel, it.uniroma2.art.lw.model.store.LingModelIOParameters)
     */
    public void storeLingModel(LingModel lm, LingModelIOParameters pars) throws LingModelStoreException {
        Document xml = new DocumentImpl();
        Element root = xml.createElement("linguistic_watermark");
        
        
        /////////////////////////
    	// selected instances  //
    	/////////////////////////
        
        Element selectedInstancesElement = XMLHelp.addNewChildElement(root, "selected_instances");
        
        //Collection<LinguisticResource> selectedInstances = lm.getSelectedInstances();
        Element selectedInstanceElement;
        //for(LinguisticResource lr : selectedInstances){
        for(LinguisticResource lr : lm.getLinguisticResources()){
        	if(lr.getLinguisticInterface() != null){
	        	selectedInstanceElement = XMLHelp.addNewChildElement(selectedInstancesElement, "instance");
	        	selectedInstanceElement.setAttribute("id", lr.getId());
        	}
        }
        
        
        
        ////////////////////////
        // interfaces section //
        ////////////////////////
        Element interfacesElement = XMLHelp.addNewChildElement(root, "interfaces");
        
        //Collection<LIFactory> liFactories = lm.getLinguisticInterfaceFactories();
        Collection<LinguisticResourceType> lRessType = lm.getLinguisticResourcesType();
        Element interfaceElement;
         
        
        try {
            //for (LIFactory liFactory : liFactories) {
        	for(LinguisticResourceType lResType : lRessType){
                interfaceElement = XMLHelp.addNewChildElement(interfacesElement, "interface");
                interfaceElement.setAttribute("id", lResType.getId());
                Collection <String> properties = lResType.getInterfaceProperties();
                //Collection<String> properties = liFactory.getInterfaceProperties();
                for (String property : properties) {
                    Element propertyElement = XMLHelp.addNewChildElement(interfaceElement,"property");
                    propertyElement.setAttribute("id", property);
                    propertyElement.setAttribute("value", lResType.getPropertyValue(property));
                    propertyElement.setAttribute("description", lResType.getPropertyDescription(property));
                }
            }
        }
        catch (PropertyNotFoundException e) {
            throw new LingModelStoreException(e);
        }
    
        ////////////////////////
        // instances section  //
        ////////////////////////
        
        Element instancesElement = XMLHelp.addNewChildElement(root, "instances_specification");
        Collection<LinguisticResource> linguisticResources = lm.getLinguisticResources();
        Element instanceElement;

        try {
            for (LinguisticResource lingRes : linguisticResources) {
                instanceElement = XMLHelp.addNewChildElement(instancesElement, "instance");
                instanceElement.setAttribute("id", lingRes.getId());
                instanceElement.setAttribute("interface", lingRes.getLinguisticInterfaceID());
                Collection<String> properties = lingRes.getProperties();
                for (String property : properties) {                
                    Element propertyElement = XMLHelp.addNewChildElement(instanceElement,"property");
                    propertyElement.setAttribute("id", property);
                    propertyElement.setAttribute("value", lingRes.getPropertyValue(property));
                    propertyElement.setAttribute("description", lingRes.getPropertyDescription(property));              
                }
            }
        }   
        catch (PropertyNotFoundException e) {
            throw new LingModelStoreException(e);
        }

        
        xml.appendChild(root);
        
        if(backendXMLFile.exists())
        	backendXMLFile.delete();
        
        try {
        	TransformerFactory tFactory = TransformerFactory.newInstance();
        	Transformer transformer = tFactory.newTransformer();
        	transformer.setOutputProperty(OutputKeys.INDENT , "yes");
			DOMSource source = new DOMSource(xml);
			StreamResult result = new StreamResult(new FileWriter(backendXMLFile));
			transformer.transform(source, result);
			
		} catch (IOException e) {
			throw new LingModelStoreException(e);
		} catch (TransformerConfigurationException e) {
		    throw new LingModelStoreException(e);
		} catch (TransformerException e) {
		    throw new LingModelStoreException(e);
		}

    }


}
