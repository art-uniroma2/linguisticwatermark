/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is Linguistic Watermark.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
 * All Rights Reserved.
 *
 * Linguistic Watermark was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
 * Current information about Linguistic Watermark can be obtained at 
 * http://art.uniroma2.it/software/LinguisticWatermark/
 *
 */

package it.uniroma2.art.lw.config.ui;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.LingModel;
import it.uniroma2.art.lw.model.LinguisticResource;
import it.uniroma2.art.lw.model.LinguisticResourceType;
import it.uniroma2.art.lw.model.objects.LIFactory;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class EditResourceFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1622678392471578382L;

	private HashMap<String, JTextField> jtextfiledMap;

	private JTextField textfieldResId;
	private ConfigureFrame parentFrame;

	public EditResourceFrame() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				parentFrame.enableFrame(true);
				dispose();
			}
		});
	}

	public void initialize(final ConfigureFrame parentFrame, final LingModel lingModel,
			final String instanceId, final String interfaceId, final boolean isInstance) {
		this.parentFrame = parentFrame;
		
		JPanel globalPanel = new JPanel();
		globalPanel.setLayout(new BorderLayout());
		GridBagLayout resourceBag = new GridBagLayout();
		JPanel panelResource = new JPanel(resourceBag);
		JScrollPane pane = new JScrollPane(panelResource);
		globalPanel.add(pane, BorderLayout.NORTH);
		BoxLayout boxLayout = new BoxLayout(panelResource, BoxLayout.Y_AXIS);
		
		panelResource.setLayout(boxLayout);
		jtextfiledMap = new HashMap<String, JTextField>();
		GridBagConstraints limResource = new GridBagConstraints();
		int instance_Y = 0;
		try {
			if (isInstance) {
				LinguisticResource lRes = lingModel.getLinguisticResource(instanceId);

				JLabel labelInstanceId = new JLabel("Instance ID = ");
				labelInstanceId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limResource.anchor = GridBagConstraints.WEST;
				limResource.fill = GridBagConstraints.NONE;
				limResource.gridx = 0;
				limResource.gridy = instance_Y;
				limResource.weightx = 1;
				limResource.gridwidth = 1;
				limResource.gridheight = 1;
				resourceBag.setConstraints(labelInstanceId, limResource);
				panelResource.add(labelInstanceId);

				textfieldResId = new JTextField(instanceId);
				textfieldResId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limResource.anchor = GridBagConstraints.WEST;
				limResource.fill = GridBagConstraints.HORIZONTAL;
				limResource.gridx = 2;
				limResource.gridy = instance_Y;
				limResource.weightx = 1;
				limResource.gridwidth = 1;
				limResource.gridheight = 1;
				resourceBag.setConstraints(textfieldResId, limResource);
				panelResource.add(textfieldResId);
				instance_Y++;

				Collection<String> instancePropertiesList = lRes.getProperties();
				for (String instancePropertyId : instancePropertiesList) {
					JLabel labelPropId = new JLabel(instancePropertyId + ":");
					limResource.anchor = GridBagConstraints.NORTHWEST;
					limResource.fill = GridBagConstraints.HORIZONTAL;
					limResource.gridx = 0;
					limResource.gridy = instance_Y;
					limResource.weightx = 1;
					limResource.weighty = 1;
					limResource.gridwidth = 1;
					limResource.gridheight = 1;
					resourceBag.setConstraints(labelPropId, limResource);
					panelResource.add(labelPropId);

					Component strut = Box.createHorizontalGlue();
					limResource.gridx = 1;
					limResource.gridy = instance_Y;
					limResource.weightx = 1;
					resourceBag.setConstraints(strut, limResource);
					panelResource.add(strut);

					int gridxTextbox = 2;
					
					String propValue = lRes.getPropertyValue(instancePropertyId);
					JTextField textFieldPropValue = new JTextField(propValue);
					
					LIFactory liFactory = lRes.getLinguisticInterfaceFactory();
					if (liFactory.isAPropertyFile(instancePropertyId)
							|| liFactory.isAPropertyDirectory(instancePropertyId)) {
						JButton button = nameFieldCreate(this, textFieldPropValue, 
								liFactory.isAPropertyDirectory(instancePropertyId));
						limResource.gridx = 3;
						limResource.gridy = instance_Y;
						limResource.weightx = 1;
						resourceBag.setConstraints(button, limResource);
						panelResource.add(button);
						gridxTextbox = 3;
					}
					
					
					textFieldPropValue.setToolTipText(lRes.getPropertyDescription(instancePropertyId));
					limResource.gridx = gridxTextbox;
					limResource.gridy = instance_Y;
					limResource.weightx = 1;
					resourceBag.setConstraints(textFieldPropValue, limResource);
					panelResource.add(textFieldPropValue);
					jtextfiledMap.put(instancePropertyId, textFieldPropValue);

					
				}
			} else {
				LinguisticResourceType lResType = lingModel.getLinguisticResourceType(interfaceId);

				JLabel labelInstanceId = new JLabel("Interface ID = ");
				labelInstanceId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limResource.anchor = GridBagConstraints.WEST;
				limResource.fill = GridBagConstraints.NONE;
				limResource.gridx = 0;
				limResource.gridy = instance_Y;
				limResource.weightx = 1;
				limResource.gridwidth = 1;
				limResource.gridheight = 1;
				resourceBag.setConstraints(labelInstanceId, limResource);
				panelResource.add(labelInstanceId);

				textfieldResId = new JTextField(interfaceId);
				textfieldResId.setEditable(false);
				textfieldResId.setFont(new Font("Microsoft Sans Serif", Font.BOLD, 11));
				limResource.anchor = GridBagConstraints.WEST;
				limResource.fill = GridBagConstraints.HORIZONTAL;
				limResource.gridx = 2;
				limResource.gridy = instance_Y;
				limResource.weightx = 1;
				limResource.gridwidth = 1;
				limResource.gridheight = 1;
				resourceBag.setConstraints(textfieldResId, limResource);
				panelResource.add(textfieldResId);
				instance_Y++;

				Collection<String> interfacePropertyList = lResType.getInterfaceProperties();
				for (String interfacePropertyId : interfacePropertyList) {
					JLabel labelPropId = new JLabel(interfacePropertyId + ":");
					limResource.anchor = GridBagConstraints.NORTHWEST;
					limResource.fill = GridBagConstraints.HORIZONTAL;
					limResource.gridx = 0;
					limResource.gridy = instance_Y;
					limResource.weightx = 1;
					limResource.weighty = 1;
					limResource.gridwidth = 1;
					limResource.gridheight = 1;
					resourceBag.setConstraints(labelPropId, limResource);
					panelResource.add(labelPropId);

					Component strut = Box.createHorizontalGlue();
					limResource.gridx = 1;
					limResource.gridy = instance_Y;
					limResource.weightx = 1;
					resourceBag.setConstraints(strut, limResource);
					panelResource.add(strut);

					int gridxTextbox = 2;
					
					String propValue = lResType.getPropertyValue(interfacePropertyId);
					JTextField textFieldPropValue = new JTextField(propValue);
					
					LIFactory liFactory = lResType.getLIFactory();
					if (liFactory.isAPropertyFile(interfacePropertyId)
							|| liFactory.isAPropertyDirectory(interfacePropertyId)) {
						JButton button = nameFieldCreate(this, textFieldPropValue, 
								liFactory.isAPropertyDirectory(interfacePropertyId));
						limResource.gridx =2;
						limResource.gridy = instance_Y;
						limResource.weightx = 1;
						resourceBag.setConstraints(button, limResource);
						panelResource.add(button);
						gridxTextbox = 3;
					}
					
					
					textFieldPropValue.setToolTipText(lResType.getPropertyDescription(interfacePropertyId));
					limResource.gridx = gridxTextbox;
					limResource.gridy = instance_Y;
					limResource.weightx = 2;
					resourceBag.setConstraints(textFieldPropValue, limResource);
					panelResource.add(textFieldPropValue);
					jtextfiledMap.put(interfacePropertyId, textFieldPropValue);

					
					instance_Y++;
				}
			}
		} catch (PropertyNotFoundException ex) {
			ex.printStackTrace();
		}
		
		
		JPanel buttonPanel = new JPanel();
		
		JButton buttonOk = new JButton("OK");
		buttonOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean changed = false;
				try {
					Collection<String> propertiesList = jtextfiledMap.keySet();
					String realInstanceId = instanceId;
					if (isInstance) {
						if (textfieldResId.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "You need to input an id for the instance");
							return;
						}
						if (!instanceId.equals(textfieldResId.getText())) {
							changed = true;
							LinguisticResource lRes = lingModel.getLinguisticResource(textfieldResId.getText());
							if (lRes != null) {
								JOptionPane.showMessageDialog(null,
										"cannot create a Linguistic Resource with the following id: " + instanceId
												+ " because there is already one");
								return;
							}
							lingModel.changeInstanceName(instanceId, textfieldResId.getText());
							realInstanceId = textfieldResId.getText();
						}
					}

					if (isInstance) {
						for (String propId : propertiesList) {
							LinguisticResource lRes = lingModel.getLinguisticResource(realInstanceId);
							String propOldValue = lRes.getPropertyValue(propId);
							JTextField textFieldPropValue = jtextfiledMap.get(propId);
							if (!propOldValue.equals(textFieldPropValue.getText())) {
								changed = true;
								lRes.getPropertyValue(propId); // this is used
																// only to see
																// if the
																// property
																// exist
								lRes.setProperty(propId, textFieldPropValue.getText());
								if (lRes.getLinguisticInterface() != null) {
									try{
										lRes.unloadLinguisticInterface();
										lRes.loadLinguisticInterface();
									}
									catch (LinguisticInterfaceLoadException e1) {
										lRes.unloadLinguisticInterface();
										// show a window displaying the message that the resource could not me loaded
										Frame frame = JOptionPane.getRootFrame();
										String errorMsg = "The Linguistic Resource "+lRes.getId()+" of "+lRes.getLinguisticInterfaceID()+" could not be loaded";
										JOptionPane.showMessageDialog(frame, errorMsg, "LinguisticResource Initialization Error!", JOptionPane.ERROR_MESSAGE);
									}
								}
							}
						}
					}
					else {
						for (String propId : propertiesList) {
							LinguisticResourceType lResType = lingModel
									.getLinguisticResourceType(interfaceId);
							String propOldValue = lResType.getPropertyValue(propId);
							JTextField textFieldPropValue = jtextfiledMap.get(propId);
							if (!propOldValue.equals(textFieldPropValue.getText())) {
								changed = true;
								lingModel.getLinguisticResourceType(interfaceId).setProperty(propId,
										textFieldPropValue.getText());
								// now we must unload an load all the instance
								// of this interface which were previously
								// loaded
								Collection<LinguisticResource> lingRess = lingModel.getLinguisticResources();
								for (LinguisticResource lingRes : lingRess) {
									if (lingRes.getLinguisticInterfaceID().equals(interfaceId)
											&& lingRes.getLinguisticInterface() != null) {
										lingRes.unloadLinguisticInterface();
										try{
											lingRes.loadLinguisticInterface();
										}
										catch (LinguisticInterfaceLoadException e1) {
											lingRes.unloadLinguisticInterface();
											// show a window displaying the message that the resource could not me loaded
											Frame frame = JOptionPane.getRootFrame();
											String errorMsg = "The Linguistic Resource "+lingRes.getId()+" of "+lingRes.getLinguisticInterfaceID()+" could not be loaded";
											JOptionPane.showMessageDialog(frame, errorMsg, "LinguisticResource Initialization Error!", JOptionPane.ERROR_MESSAGE);
										}
									}
								}
							}
						}
					}

				} catch (PropertyNotFoundException e) {
					e.printStackTrace();
				}/* catch (LinguisticInterfaceLoadException e) {
					e.printStackTrace();
				}*/
				if (changed)
					LinguisticWatermarkManager.save();
				dispose();
				parentFrame.enableFrame(true);

			}
		});
		limResource.gridx = 1;
		limResource.gridy = instance_Y;
		limResource.weightx = 2;
		resourceBag.setConstraints(buttonOk, limResource);
		buttonPanel.add(buttonOk);
		
		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				parentFrame.enableFrame(true);
			}
		});
		limResource.gridx = 2;
		limResource.gridy = instance_Y;
		limResource.weightx = 2;
		resourceBag.setConstraints(buttonCancel, limResource);
		buttonPanel.add(buttonCancel);
		
		globalPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		JPanel resourcePanelContainer = new JPanel();
		resourcePanelContainer.setLayout(new BoxLayout(resourcePanelContainer, BoxLayout.Y_AXIS));
		//resourcePanelContainer.add(panelResource);
		resourcePanelContainer.add(globalPanel);
		
		
		this.getContentPane().add(resourcePanelContainer);
		pack();
		//setSize(400, 300);
	}

	private JButton nameFieldCreate(final Component frame, final JTextField textField,
			final boolean  isDir) {
		Icon icon = new ImageIcon("plugins/it.uniroma2.art.ontoling/properties/TreeClosed.gif");
		JButton nameField = new JButton(icon);
		nameField.setSize(icon.getIconHeight(), icon.getIconWidth());
		Action findAction = new AbstractAction("FindFile") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 7349519913720613622L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("./"));
				if (isDir)
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = chooser.showOpenDialog(frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File chosen = chooser.getSelectedFile();
					textField.setText(chosen.toString());
				}
			}
		};
		nameField.addActionListener(findAction);
		return nameField;
	}

}
