 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is Linguistic Watermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/LinguisticWatermark/
  *
  */


package it.uniroma2.art.lw.model;

import it.uniroma2.art.lw.exceptions.LingModelStoreException;
import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.store.LingModelIO;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class LingModel {
    
    private HashMap<String, LinguisticResource> linguisticResources;
    //private HashMap<String, LIFactory> linguisticInterfaceFactories;
    private HashMap<String, LinguisticResourceType> linguisticResourcesType;
    
    
    //private Collection<LinguisticResource> selectedInstances = new Vector<LinguisticResource>();
    
    private LingModelIO lmIO;
    
    LingModel(HashMap<String, LIFactory> linguisticIntefaceFactories) {
        linguisticResources = new HashMap<String, LinguisticResource>();
        linguisticResourcesType = new HashMap<String, LinguisticResourceType>();
        
        
        for(String idFactory : linguisticIntefaceFactories.keySet()){
        	LinguisticResourceType lResType = new LinguisticResourceType(idFactory);
        	lResType.setLIFactory(linguisticIntefaceFactories.get(idFactory));
        	linguisticResourcesType.put(idFactory, lResType);
        }
        
        //this.linguisticInterfaceFactories = linguisticIntefaceFactories;
    }
    
    /**
     * @return the set of identifiers of all the available Linguistic Interfaces 
     */
    public Collection<String> getLinguisticInterfacesIDs() {
    	Collection<String> idsInterfaces = new Vector<String>();
    	for(LinguisticResourceType lResType : linguisticResourcesType.values()){
    		if(lResType.getLIFactory() != null){
    			idsInterfaces.add(lResType.getId());
    		}
    	}
    	return idsInterfaces;
    	//return linguisticInterfaceFactories.keySet();
    }
    
    public Collection<String> getInstancesIDs(){
    	return linguisticResources.keySet();
    }
    
    /**
     * @return all the LinguisticResourceType
     */
    public Collection<LinguisticResourceType> getLinguisticResourcesType() {
    	return linguisticResourcesType.values();
    	/*
    	Collection<LinguisticResourceType> lRessType = new Vector<LinguisticResourceType>();
    	for(LinguisticResourceType lResType : linguisticResourcesType.values()){
    		if(lResType.getLIFactory() != null){
    			
    			//liFactories.add(lResType.getLIFactory());
    		}
    	}
    	return liFactories;
    	*/
        //return linguisticInterfaceFactories.values();
    }

    /**
     * @param id
     * @return a LinguisticResourceType through its identifier or null if that LinguisticResourceType does not exist
     */
    public LinguisticResourceType getLinguisticResourceType(String id) {
    	return linguisticResourcesType.get(id);
    	/*
    	LinguisticResourceType lResType = linguisticResourcesType.get(id);
    	if(lResType == null){
    		return null;
    	}
    	return lResType.getLIFactory();
    	*/
    	//return linguisticInterfaceFactories.get(id);
    }
    
    public void addLinguisticResourceType(LinguisticResourceType lResType){
    	linguisticResourcesType.put(lResType.getId(), lResType);
    }
    
    
    /**
     * 
     * @return all the LIFactory that exist
     */
    public Collection<LIFactory> getLIFactoriesLoaded(){
    	Collection<LIFactory> liFactories = new Vector<LIFactory>();
    	for(LinguisticResourceType lResType : linguisticResourcesType.values()){
    		if(lResType.getLIFactory() != null){
    			liFactories.add(lResType.getLIFactory());
    		}
    	}
    	
    	return liFactories;
    }
    
    /**
     * @return the set of linguistic resources installed in the hosting system
     */
    public Collection<LinguisticResource> getLinguisticResources() {
        return linguisticResources.values();
    }
    
    /**
     * @param id
     * @return the linguistic resource, identified by <code>id</code>, which is installed in the hosting system
     */
    public LinguisticResource getLinguisticResource(String id) {
        return linguisticResources.get(id);
    }
    
    /**
     * @param lr
     */
    public void addLinguisticResourse(LinguisticResource lr){
    	if(!linguisticResources.containsKey(lr.getId())){
    		linguisticResources.put(lr.getId(), lr);
    	}
    }
    
    /**
     * @param idLR
     * @return
     */
    public LinguisticResource removeLinguisticResource(String idLR){
    	return linguisticResources.remove(idLR);
    }
    
    
    
    
    public void unloadLoadedInstances(){
    	for(LinguisticResource lRes : linguisticResources.values()){
    		lRes.unloadLinguisticInterface();
    		//LinguisticInterface lInt = lRes.getLinguisticInterface();
    	}
    }
    
    /**
     * @param lrID
     * @return true if exist a LinguisticResource with that id, false otherwise
     */
    /*
    public boolean addSelectedInstances(String lrID){
    	LinguisticResource lr = getLinguisticResource(lrID);
    	if(lr != null){
    		selectedInstances.add(lr);
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    */
    
    public void setLMIO(LingModelIO lmIO){
    	this.lmIO = lmIO;
    }
    
    public void save(File file) throws LingModelStoreException{
    	if(file != null){
    		lmIO.setFile(file);
    	}
    	
    	lmIO.storeLingModel(this, null);
    }
    /**
     * 
     * @return the numer of LinguisticResource which are loaded in the Model
     */
    public int getNumLoadedLInt(){
    	int num=0;
    	for(LinguisticResource lRes : linguisticResources.values()){
    		if(lRes.getLinguisticInterface() != null){
    			++num;
    		}
    	}
    	
    	return num;
    }
    
    public void changeInstanceName(String oldName, String newName){
    	LinguisticResource lRes = linguisticResources.get(oldName);
    	if (lRes == null)
    		return;
    	linguisticResources.remove(oldName);
    	lRes.setId(newName);
    	linguisticResources.put(newName, lRes);
    }
    
}
