/*
 * Created on 19-mag-2005
 */
package it.uniroma2.art.lw.exceptions;

import java.io.IOException;

/**
 * @author Armando Stellato
 * 
 * Exception thrown when a Linguistic Watermark configuration file (whichever format is being used) is not found
 */
public class LWNotFoundException extends IOException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1672812243964874137L;
	public LWNotFoundException(String msg) {
        super(msg);
    }
	public LWNotFoundException() {
        super("Resource could not be loaded!");
    }
}
