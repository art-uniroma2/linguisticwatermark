 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is LinguisticWatermark.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * LinguisticWatermark was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about LinguisticWatermark can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.lw.model.objects;

import java.util.Collection;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public interface TaxonomicalLR extends ConceptualizedLR {
    /**
     * @param c a given word sense
     * @return all the direct supersenses of <code>c</code>
     */
    public Collection<SemanticIndex> getDirectAncestors(SemanticIndex c);

    /**
     * @param c a given word sense
     * @return all the direct subsenses of <code>c</code>
     */
    public Collection<SemanticIndex> getDirectDescendants(SemanticIndex c);

}
