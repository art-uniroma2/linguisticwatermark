 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is WordNetInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/lw/wordnetinterface
  *
  */


 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

/*
 * WordnetInterface.java
 *
 * Created on 30 marzo 2004, 11.47
 */
package it.uniroma2.art.lw.wordnet;


import it.uniroma2.art.jmwnl.ewn.JMWNL;
import it.uniroma2.art.jmwnl.ewn.data.EWNPointerTypes;
import it.uniroma2.art.jmwnl.ewn.data.EWNPointerUtils;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchStrategy;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.lw.model.objects.TaxonomicalLR;
import it.uniroma2.art.lw.model.objects.WholeWordToggling;
import it.uniroma2.art.lw.properties.DirectoryProperty;
import it.uniroma2.art.lw.properties.InstanceProperty;

import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JOptionPane;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.PointerType;
import net.didion.jwnl.data.PointerUtils;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.data.list.PointerTargetNode;
import net.didion.jwnl.data.list.PointerTargetNodeList;
import net.didion.jwnl.data.list.PointerTargetTreeNode;
import net.didion.jwnl.data.list.PointerTargetTreeNodeList;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.dictionary.MorphologicalProcessor;
import net.didion.jwnl.util.Resolvable;

/**
 *
 * @author  Armando Stellato
 */
public class WordnetInterface extends LinguisticInterface implements TaxonomicalLR, LRWithGlosses, WholeWordToggling
{
    //private int resultsTablecolumns = GUI_Facade.resultsTablecolumns;
    protected double wnVersion;
    private GetWordnetConcept gwc;
    private GetSynsetFromTree gst;
    
    private Collection<SemanticRelation> semRelList;
    private Collection<LexicalRelation> lexRelList;
    private Collection<SearchStrategy> searchStratList;
    private Collection<SearchFilter> searchFilterList;
    
    private HashMap<String, PointerType> relationPointerTypeMap; 
    private HashMap<String, String> lexicalLabelRelationMap;
    private HashMap<String, String> semanticLabelRelationMap;
    
    
    
    @InstanceProperty (description="search path for the jwnl xml property file")
    @DirectoryProperty
    public String jwnl_properties;
    
    
    //private boolean wholeWordSearchStatus; // TODO vedere se deve stare qui o da un altro posto
    
    public void initialize() throws LinguisticInterfaceLoadException{
    	System.out.println("Initializing Wordnet!!!");
    	System.out.println("jwnl_properties = "+ jwnl_properties); // da cancellare
    	//try {   JWNL.initialize(new FileInputStream(new File(jwnl_properties).getAbsolutePath())); }        
    	try {   JMWNL.initialize(new File(jwnl_properties)); }        
        catch (Exception ex) {	
            						/*TODO I shoul change initialize() in LWatermark to throw exceptions, which may be caught by the application using the Linguistic Watermark package*/
            						//          custom title, error icon
            						String errorMsg = "some problems occurred during wordnet initialization\ncheck file_properties.xml for\n" +
									"- wordnet version\n" +
									"- wordnet language\n" +
									"- correct location of wordnet dict folder\n" +
									"- correct dictionary element factory specification; use:\n" +
									"         - PrincetonWN17FileDictionaryElementFactory for wordnet 2.0 and wordnet 1.7.X\n" +
									"         - PrincetonWN16DictionaryElementFactory for wordnet 1.6";
									
									
            						
            						Frame frame = JOptionPane.getRootFrame();
            						JOptionPane.showMessageDialog(frame, errorMsg, "Wordnet Initialization Error!", JOptionPane.ERROR_MESSAGE);
            						throw new LinguisticInterfaceLoadException(errorMsg);
            				}
        
        language = JWNL.getVersion().getLocale().getLanguage();
        wnVersion = JWNL.getVersion().getNumber();
        
        setRelation();
        
        setSearchFilters();
        
        setSearchStrategy();
        
		shortID = "wn-" + wnVersion + "-" + language;
		System.out.println(language);
		System.out.println(wnVersion);
		System.out.println(shortID);
		
		setWholeWordSearchStatus(false);
		
		gwc = new GetWordnetConcept();
		gst = new GetSynsetFromTree();
    }
   

    /****************************************************************/    
    /****           ConceptualizedLR INTERFACE METHODS           ****/    
    /****************************************************************/   
    
    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.ConceptualizedLR#getConceptLexicals(it.uniroma2.info.ai_nlp.LinguisticWatermark.Concept)
     */
    public String[] getConceptLexicals(SemanticIndex c) {
        Synset synset = ((WordnetConcept)c).getSynset();
        Word[] words = synset.getWords();
        String[] Lexicals = new String[words.length];
        for(int i = 0;i<words.length;i++)
            Lexicals[i] = words[i].getLemma();
        return Lexicals;        
    }

    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.ConceptualizedLR#getConcept(java.lang.String)
     */
    public SemanticIndex getConcept(String POSSynset) throws SemIndexRetrievalException{
        WordnetConcept synset = new WordnetConcept(POSSynset);
        return synset;
    }
    
    
    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.ConceptualizedLR#getConcepts(java.lang.String)
     */
    public Collection getConcepts(SearchWord searchWord) {
        
    	ArrayList conceptsList = new ArrayList();
   		if(searchWord != null){
	    	String term = searchWord.getName();
	    	IndexWord retrievedWord = null;
	   		try {
	            buildConceptsList(term, POS.NOUN, conceptsList);
	       		buildConceptsList(term, POS.VERB, conceptsList);
	       		buildConceptsList(term, POS.ADVERB, conceptsList);
	       		buildConceptsList(term, POS.ADJECTIVE, conceptsList);
	        } catch (JWNLException e) { e.printStackTrace();  }
   		}
    	return conceptsList;        
    }

    
    private void buildConceptsList(String term, POS pos, ArrayList conceptsList) throws JWNLException {
        IndexWord retrievedWord = null;
        Synset[] synsets;
        retrievedWord = Dictionary.getInstance().getIndexWord(pos, term);
		/*
        if (exactMatch==true)
			retrievedWord = Dictionary.getInstance().getIndexWord(pos, term);
		else 
		    retrievedWord = Dictionary.getInstance().lookupIndexWord(pos, term);
		*/
		if (retrievedWord != null)
		{	
			synsets = retrievedWord.getSenses();
		    for(int i=0;i<synsets.length;i++)
		        conceptsList.add(new WordnetConcept(synsets[i]));
		}
		else { }//GUI_Facade.notifiedSearchTextFieldModify(term); }
		//TODO si potrebbe cambiare per dire che non ha trovato la parola originale ma un altra...
    }


    /****************************************************************/    
    /****           TaxonomicalLR INTERFACE METHODS              ****/    
    /****************************************************************/      
    
    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.TaxonomicalLR#getDirectAncestors(it.uniroma2.info.ai_nlp.LinguisticWatermark.Concept)
     */
	public Collection getDirectAncestors(SemanticIndex c) {
        return  getByWordnetConceptualRelation((WordnetConcept)c, "getDirectHypernyms"); 
    };

	/**
	 * @param c
	 * @return
	 */
	public Collection getAncestors(SemanticIndex c) {
	    try {
            return PointerUtils.getInstance().getHypernymTree(((WordnetConcept)c).getSynset()).getAllMatches(gwc);
        } catch (JWNLException e) {
            e.printStackTrace();
            return null;
        } 
    };
    
    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.TaxonomicalLR#getDirectDescendants(it.uniroma2.info.ai_nlp.LinguisticWatermark.Concept)
     */
    public Collection getDirectDescendants(SemanticIndex c) {
    	return  getByWordnetConceptualRelation((WordnetConcept)c, "getDirectHyponyms"); 
    }


    /**
     * @param c
     * @return
     */
    public Collection getDescendants(SemanticIndex c) {
	    try {
            return PointerUtils.getInstance().getHyponymTree(((WordnetConcept)c).getSynset()).getAllMatches(gwc);
        } catch (JWNLException e) {
            e.printStackTrace();
            return null;
        } 
    }    
    
//  TODO I tried also the method below, which is commented, but the retrieval is not complete!
    public boolean isMoreSpecific(SemanticIndex cl, SemanticIndex ch) {
        
        Synset sl = ((WordnetConcept)cl).getSynset();
        Synset sh = ((WordnetConcept)ch).getSynset();
        Collection ancestors;
	    try {
            ancestors = PointerUtils.getInstance().getHypernymTree(sl).getAllMatches(gst);
        } catch (JWNLException e) {
            e.printStackTrace();
            ancestors = new ArrayList();
        }         
        
        return ancestors.contains(sh);	
    }      
    
    
/*
    // TODO questo metodo NON � completo!!! usare nel caso, per confronto, gli altri metodi
    // che ho scritto in prolog: esempio: ms('noun.6905654', 'noun.25950').
    public boolean isMoreSpecific(Concept cl, Concept ch) {
        Synset sl = ((WordnetConcept)cl).getSynset();
        Synset sh = ((WordnetConcept)ch).getSynset();
        try {
            RelationshipList list = RelationshipFinder.getInstance().findRelationships(sl, sh, PointerType.HYPERNYM);
            for (Iterator itr = list.iterator(); itr.hasNext();) {
    			((Relationship) itr.next()).getNodeList().print();
    		}
           if (!list.isEmpty())
            {    
	    		System.out.println("Common Parent Index: " + ((AsymmetricRelationship) list.get(0)).getCommonParentIndex());
	    		System.out.println("Depth: " + ((Relationship) list.get(0)).getDepth());
	    		System.out.println("Type: " + ((Relationship) list.get(0)).getType());
            }
            return ( 
                    	!list.isEmpty() && 
                    	( 
                    	    ((Relationship) list.get(0)).getDepth()
                    	    ==
                    	    ((AsymmetricRelationship) list.get(0)).getCommonParentIndex() 
                    	) 
                   );	//the idea is that if the inversion point in the relation path is equal to the depth, then it is a direct ascension from cl to ch
        } catch (JWNLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        
    }        
*/    
     
    
    
	private class GetWordnetConcept implements PointerTargetTreeNodeList.Operation
	{			
		public Object execute(PointerTargetTreeNode node)
		{						
			return new WordnetConcept(node.getSynset()); 
		} 	
	}	     

	private class GetSynsetFromTree implements PointerTargetTreeNodeList.Operation
	{	
		public Object execute(PointerTargetTreeNode node) {
			return node.getSynset(); 
		} 	
	}	     	
	
    /****************************************************************/    
    /****           LRWithGlosses INTERFACE METHODS              ****/    
    /****************************************************************/    

    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.LRWithGlosses#getConceptGloss(it.uniroma2.info.ai_nlp.LinguisticWatermark.Concept)
     */
    public String getConceptGloss(SemanticIndex c) {
    	String gloss ="";
    	if(c instanceof WordInWordnetConcept){
    		gloss = ((WordInWordnetConcept)c).getWord().getLemma()+" : "+((WordnetConcept)c).getSynset().getGloss();
    	}
    	else if(c instanceof WordnetConcept){
    		Synset synset = ((WordnetConcept)c).getSynset();
    		gloss = synset.getGloss();
    	}
    	return gloss;
    }
    Word a;
    public String getWord(SemanticIndex c){
    	if(c instanceof WordInWordnetConcept)
    		return ((WordInWordnetConcept)c).getWord().getLemma();
    	else
    		return null;
    }
    
    /******************************************************/    
    /****           INTERFACE ACCESS METHODS           ****/    
    /******************************************************/       
    
   
    /******************************************************/    
    /****           PHYSICAL ACCESS METHODS            ****/    
    /******************************************************/    



    
    
    private Collection<SemanticIndex> getByWordnetConceptualRelation(WordnetConcept c, String Method) {
        System.out.println("Entering request");
        ArrayList <SemanticIndex>result = new ArrayList<SemanticIndex>();

        try    
        {
            Synset synset = c.getSynset();
//          System.out.println("here are conceptual relations about: " + pos_synset);
            //return (String[][])(this.getClass().getMethod("exploreWordRelation_"+Relation, parametersTypes).invoke(this,parameters));
            Class[] parametersTypes = { Class.forName("net.didion.jwnl.data.Synset") };
            Synset[] parameters = {synset};
            PointerTargetNodeList synsetsList;
            if(JMWNL.isEWN()){
            	synsetsList = (PointerTargetNodeList)EWNPointerUtils.class.getMethod(Method,parametersTypes).invoke(EWNPointerUtils.getInstance(), parameters);
            }
          	else
            	synsetsList = (PointerTargetNodeList)PointerUtils.class.getMethod(Method,parametersTypes).invoke(PointerUtils.getInstance(), parameters);
            
            
            
            
            java.util.ListIterator sensesIterator = synsetsList.listIterator();
            synsetsList.print();
            System.out.println(synsetsList.size());
            while (sensesIterator.hasNext())
                result.add(	new WordnetConcept(	((PointerTargetNode)sensesIterator.next()).getSynset()	)	);
            	//era precedentemente: WordnetConcept.getPOSSynsetID(tempsynset);
        }
        catch(Exception e) {e.printStackTrace();}
        return result;     
    }      
     
    /*
    private Collection<SemanticIndex> getByWordnetLexicalRelation(WordnetConcept c, String word, String Method) {
    	System.out.println("Entering request");
        ArrayList <SemanticIndex> result = new ArrayList<SemanticIndex>();
        try    
        {
            Synset synset = c.getSynset();
            Class[] parametersTypes = { Class.forName("net.didion.jwnl.data.Word") };
            Word[] parameters = {null};
            
            for(Word tempWord: c.getSynset().getWords()){
            	if(tempWord.getLemma().compareToIgnoreCase(word) == 0){
            		parameters[0] = tempWord;
            		break;
            	}
            }
            if(parameters[0] == null ){
            	return result;
            }
            PointerTargetNodeList synsetsWordsList;
            
            synsetsWordsList = (PointerTargetNodeList)EWNPointerUtils.class.getMethod(Method,parametersTypes).invoke(EWNPointerUtils.getInstance(), parameters);
                       
            
            java.util.ListIterator sensesIterator = synsetsWordsList.listIterator();
            synsetsWordsList.print();
            System.out.println(synsetsWordsList.size());
            while (sensesIterator.hasNext()){
            	PointerTargetNode pointerTargetNode = (PointerTargetNode)sensesIterator.next();
                result.add(	new WordInWordnetConcept(pointerTargetNode.getSynset(), pointerTargetNode.getWord().getLemma()	)	);
            }
        }
        catch(Exception e) {e.printStackTrace();}
        return result;   
    }
    */
    
    public String[] getConceptLexicals(String pos_synset) {
        Synset synset;
        String[] Lexicals;
        try {
            synset = (new WordnetConcept(pos_synset)).getSynset();
        } catch (SemIndexRetrievalException e) {
            Lexicals = new String[0];
            return Lexicals;
        }
        Word[] words = synset.getWords();
        Lexicals = new String[words.length];
        for(int i = 0;i<words.length;i++)
            Lexicals[i] = words[i].getLemma();
        return Lexicals;
    }
    
    public Iterator getSynsetIterator() { 
        return new SynsetIterator();
    }
    

    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.WholeWordToggling#setWholeWordSearchStatus(boolean)
     */
    public void setWholeWordSearchStatus(boolean status) {
    	for(SearchFilter srchFilter : searchFilterList){
			if(srchFilter.getName().compareTo("Whole word search") == 0)
				srchFilter.setActive(status);
		}
    	System.out.println(status);
    }

    public static void main (String[] args) throws LinguisticInterfaceLoadException {
        
        WordnetInterface wn = new WordnetInterface();
        
        wn.jwnl_properties = "D:/DEVELOPMENT/PROTEGE/Protege_3.2.1/plugins/it.uniroma2.info.ai-nlp.ontoling/WordnetInterface/file_properties_EWNItalian.xml";
        //wn.jwnl_properties = new JWNLProperties("jwnl_properties", "D:/DEVELOPMENT/PROTEGE/Protege_3.2.1/plugins/it.uniroma2.info.ai-nlp.ontoling/WordnetInterface/file_properties_2-1.xml");
        
        wn.initialize();
        
        Collection results = ((ConceptualizedLR)wn).getConcepts(new SearchWord("corsa"));
        Iterator resIterator = results.iterator();
        SemanticIndex c;
        String[] tableRow;
        while (resIterator.hasNext())
        {
            c = (SemanticIndex)resIterator.next();
            System.out.println("concept: " + c);
            System.out.println("synset for concept: " + ((WordnetConcept)c).getSynset());
            System.out.println(c.getConceptRepresentation());
            System.out.println(((LRWithGlosses)wn).getConceptGloss(c));
        }
        
/*        
        Iterator sit = wn.getSynsetIterator();
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("c:\\mario.pl"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
        Synset syn;
        while (sit.hasNext()) {
            	syn = (Synset)sit.next();
                bw.write("gloss('" + syn.getGloss() + "').\n");                
        }
        bw.close();
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }	
*/        
    }
    
    class SynsetIterator implements Iterator {

        Iterator currentIterator;
        Iterator nounIterator;
        Iterator verbIterator;
        Iterator adjIterator;
        Iterator advIterator;
        
        public SynsetIterator() {
            Dictionary dictionary = Dictionary.getInstance();
            try {
                nounIterator = dictionary.getSynsetIterator(POS.NOUN);
                verbIterator = dictionary.getSynsetIterator(POS.VERB);
                adjIterator = dictionary.getSynsetIterator(POS.ADJECTIVE);
                advIterator = dictionary.getSynsetIterator(POS.ADVERB);
                currentIterator = nounIterator; 
            } catch (JWNLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        boolean hasNextIterator() {
            if (currentIterator == nounIterator || currentIterator == verbIterator || currentIterator == adjIterator)
                return true;
            else return false;
        }
        
        
        Iterator nextIterator() {
            if (currentIterator == nounIterator)
                return verbIterator;
                else if (currentIterator == verbIterator)
                    return adjIterator;
                    else if (currentIterator == adjIterator)
                        return advIterator;
                    	else return null;
        }
        
        /* (non-Javadoc)
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            if (currentIterator.hasNext() || hasNextIterator())
            	return true;
           	else return false;
        }

        /* (non-Javadoc)
         * @see java.util.Iterator#next()
         */
        public Object next() {
            if (currentIterator.hasNext())
            	return currentIterator.next();
            else if (hasNextIterator())
            	{
                	currentIterator = nextIterator();
                	return currentIterator.next(); 
            	}
            	else return null;
        }

        /* (non-Javadoc)
         * @see java.util.Iterator#remove()
         */
        public void remove() {
            // TODO Auto-generated method stub
            // DONOTHING: NOT IMPLEMENTED
        }
        
    }

    private void setSearchFilters(){
    	searchFilterList = new ArrayList<SearchFilter>();
    	SearchFilter searchFilter;
    	searchFilter = new SearchFilter("Exact search");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    	searchFilter = new SearchFilter("Polysemy_NOUN");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    	searchFilter = new SearchFilter("Polysemy_VERB");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    	searchFilter = new SearchFilter("Polysemy_ADJECTIVE");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    	searchFilter = new SearchFilter("Polysemy_ADVERB");
    	searchFilter.setActive(true);
    	searchFilterList.add(searchFilter);
    }
    
    private void setLexicalRelationsMap(){
    	lexicalLabelRelationMap = new HashMap<String, String>();
    	
    	lexicalLabelRelationMap.put((new Resolvable("ANTONYM")).toString(), "Antonymy");
    	lexicalLabelRelationMap.put((new Resolvable("PERTAINYM")).toString(), "Pertainymy");
    	lexicalLabelRelationMap.put((new Resolvable("PARTICIPLE_OF")).toString(), "Participle");
    	lexicalLabelRelationMap.put((new Resolvable("SEE_ALSO")).toString(), "Also see");
    	lexicalLabelRelationMap.put((new Resolvable("NOMINALIZATION")).toString(), "Derivation");
    }
    
    private void setSemanticRelationsMap(){
    	semanticLabelRelationMap = new HashMap<String, String>();
    	
    	semanticLabelRelationMap.put((new Resolvable("HYPERNYM")).toString(),"Hypernymy");
    	semanticLabelRelationMap.put((new Resolvable("HYPONYM")).toString(),"Hyponymy");
    	semanticLabelRelationMap.put((new Resolvable("INSTANCE_HYPERNYM")).toString(),"Instance Hypernymy");
    	semanticLabelRelationMap.put((new Resolvable("INSTANCES_HYPONYM")).toString(),"Instance Hyponymy");
    	semanticLabelRelationMap.put((new Resolvable("ATTRIBUTE")).toString(),"Attributes");
    	semanticLabelRelationMap.put((new Resolvable("CAUSE")).toString(),"Cause");
    	semanticLabelRelationMap.put((new Resolvable("ENTAILMENT")).toString(),"Entailment");
    	//semanticLabelRelationMap.put("")).toString(),"Holonymy");
    	//semanticLabelRelationMap.put("","Meronymy");
    	semanticLabelRelationMap.put((new Resolvable("MEMBER_HOLONYM")).toString(),"Member Holonymy");
    	semanticLabelRelationMap.put((new Resolvable("MEMBER_MERONYM")).toString(),"Member Meronymy");
    	semanticLabelRelationMap.put((new Resolvable("PART_HOLONYM")).toString(),"Part Holonymy");
    	semanticLabelRelationMap.put((new Resolvable("PART_MERONYM")).toString(),"Part Meronymy");
    	semanticLabelRelationMap.put((new Resolvable("SUBSTANCE_HOLONYM")).toString(),"Substance Holonymy");
    	semanticLabelRelationMap.put((new Resolvable("SUBSTANCE_MERONYM")).toString(),"Substance Meronymy");
    	semanticLabelRelationMap.put((new Resolvable("DERIVED")).toString(),"HasDerived");
    	semanticLabelRelationMap.put((new Resolvable("IN_MANNER")).toString(),"InManner");
    	semanticLabelRelationMap.put((new Resolvable("BELONGS_TO_CLASS")).toString(),"Belongs To Class");
    	semanticLabelRelationMap.put((new Resolvable("CO_AGENT_INSTRUMENT")).toString(),"CoAgent Instrument");
    	semanticLabelRelationMap.put((new Resolvable("CO_INSTRUMENT_AGENT")).toString(),"CoInstrument Agent");
    	semanticLabelRelationMap.put((new Resolvable("CO_ROLE")).toString(),"CoRole");
    	semanticLabelRelationMap.put((new Resolvable("FUZZYNYM")).toString(),"Fuzzynym");
    	semanticLabelRelationMap.put((new Resolvable("HAS_HOLO_LOCATION")).toString(),"Has Holo Location");
    	semanticLabelRelationMap.put((new Resolvable("HAS_HOLO_MADEOF")).toString(),"Has HoloMadeof");
    	semanticLabelRelationMap.put((new Resolvable("HAS_HOLONYM")).toString(),"Has Holonymy");
    	semanticLabelRelationMap.put((new Resolvable("HAS_MERO_LOCATION")).toString(),"Has Mero Location");
    	semanticLabelRelationMap.put((new Resolvable("HAS_MERO_MADEOF")).toString(),"Has Mero Made of");
    	semanticLabelRelationMap.put((new Resolvable("HAS_MERONYM")).toString(),"Has Meronymy");
    	semanticLabelRelationMap.put((new Resolvable("ROLE")).toString(),"Role");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_AGENT")).toString(),"Role Agent");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_INSTRUMENT")).toString(),"Role Instrument");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_PATIENT")).toString(),"Role Patient");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_RESULT")).toString(),"Role Result");
    	semanticLabelRelationMap.put((new Resolvable("DERIVATION")).toString(),"Derivation");
    	semanticLabelRelationMap.put((new Resolvable("STATE_OF")).toString(), "State Of");
    	semanticLabelRelationMap.put((new Resolvable("IS_DERIVED_FROM")).toString(),"Is Derived From");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_DIRECTION")).toString(),"Role Direction");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_LOCATION")).toString(),"Role Location");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_SOURCE_DIRECTION")).toString(),"Role Source Direction");
    	semanticLabelRelationMap.put((new Resolvable("ROLE_TARGET_DIRECTION")).toString(),"Role Target Direction");
    	semanticLabelRelationMap.put((new Resolvable("BE_IN_STATE")).toString(),"Be In State");
    	semanticLabelRelationMap.put((new Resolvable("HAS_SUBEVENT")).toString(),"Has Subevent");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED")).toString(),"Involved");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_AGENT")).toString(),"Involved Agent");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_INSTRUMENT")).toString(),"Involved Instrument");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_LOCATION")).toString(),"Involved Location");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_PATIENT")).toString(),"Involved Patient");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_RESULT")).toString(),"Involved Result");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_SOURCE_DIRECTION")).toString(),"Involved Source Direction");
    	semanticLabelRelationMap.put((new Resolvable("IS_SUBEVENT_OF")).toString(),"Is Subevent Of");
    	semanticLabelRelationMap.put((new Resolvable("IS_CAUSED_BY")).toString(),"Is Caused By");
    	semanticLabelRelationMap.put((new Resolvable("XPOS_FUZZYNYM")).toString(),"Xpos Fuzzynym");
    	semanticLabelRelationMap.put((new Resolvable("XPOS_HYPONYM")).toString(),"Has Xpos Hyponymy");
    	semanticLabelRelationMap.put((new Resolvable("XPOS_HYPERNYM")).toString(),"Has Xpos Hyperonymy");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_DIRECTION")).toString(),"Involved Direction");
    	semanticLabelRelationMap.put((new Resolvable("INVOLVED_TARGET_DIRECTION")).toString(),"Involved Target Direction");
    }
    
    
    private void setRelation(){
    	setLexicalRelationsMap();
    	setSemanticRelationsMap();
    	boolean pointerAdded = false;
    	
    	relationPointerTypeMap = new HashMap<String, PointerType>();
    	semRelList = new ArrayList<SemanticRelation>();
    	lexRelList = new ArrayList<LexicalRelation>();
    	
    	String relationString="";
    	
    	List<PointerType> pointerTypList;
    	
    	if(JMWNL.isEWN()){
    		pointerTypList = EWNPointerTypes.getAllPointerTypes();
    	}
    	else{
    		pointerTypList = PointerType.getAllPointerTypes();
    	}
    	
    	for(PointerType pointerType : pointerTypList){
    		if(semanticLabelRelationMap.containsKey(pointerType.getLabel())){
    			relationString = semanticLabelRelationMap.get(pointerType.getLabel());
    			semRelList.add(new SemanticRelation(relationString));
    			pointerAdded = true;
    		}
    		if(lexicalLabelRelationMap.containsKey(pointerType.getLabel())){
    			relationString = lexicalLabelRelationMap.get(pointerType.getLabel());
    			lexRelList.add(new LexicalRelation(relationString));
    			pointerAdded = true;
    		}
    		if(pointerAdded){
    			pointerAdded = false;
    			relationPointerTypeMap.put(relationString, pointerType);
    		}
    	}
    }
    
    

    private void setSearchStrategy(){
    	searchStratList = new ArrayList<SearchStrategy>();
    	searchStratList.add(new SearchStrategy("all lemma"));
    }
 
    
    
	@Override
	public LexicalRelation getLexicalRelation(String lexRelation) {
		for(LexicalRelation lexRel : lexRelList){
			if(lexRel.getName().compareTo(lexRelation) == 0)
				return lexRel;
		}
		return null;
	}


	@Override
	public Collection<LexicalRelation> getLexicalRelations() {
		return lexRelList;
	}


	@Override
	public SearchFilter getSearchFilter(String searchFilter) {
		for(SearchFilter srchFilter : searchFilterList){
			if(srchFilter.getName().compareTo(searchFilter) == 0)
				return srchFilter;
		}
		return null;
	}


	@Override
	public Collection<SearchFilter> getSearchFilters() {
		return searchFilterList;
	}


	@Override
	public Collection<SearchStrategy> getSearchStrategies() {
		return searchStratList;
	}


	@Override
	public SearchStrategy getSearchStrategy(String searchStrategy) {
		for(SearchStrategy searchStrat : searchStratList){
			if(searchStrat.getName().compareTo(searchStrategy) == 0)
				return searchStrat;
		}
		return null;
	}


	@Override
	public SemanticRelation getSemanticRelation(String semRelation) {
		for(SemanticRelation semRel : semRelList){
			if(semRel.getName().compareTo(semRelation) == 0)
				return semRel;
		}
		return null;
	}


	@Override
	public Collection<SemanticRelation> getSemanticRelations() {
		return semRelList;
	}


	@Override
	protected Collection<SearchWord> getSearchWords(String word,
			String methodName, SearchFilter... searchFilters) {
		
		Collection<SearchWord> searchedWord = new ArrayList<SearchWord>();
		IndexWord indexWord = null;
		Collection<String> searchFilterString = new ArrayList<String>();
		
		// prepare the filter that should be use during the search
		if(searchFilters == null) {
			for(SearchFilter searchFilter : searchFilterList) {
				if(searchFilter.isActive()) {
					searchFilterString.add(searchFilter.getName());
				}
			}
		}
		else { 
			for(SearchFilter searchFilter : searchFilters) {
				if(searchFilter == SearchFilter.ANY){
					//Use all the filter of the linguistic resource
					searchFilterString = new ArrayList<String>();
					for(SearchFilter searchFilterAny : searchFilterList) {
						searchFilterString.add(searchFilterAny.getName());
					}
					break;
				}
				else if(searchFilter == SearchFilter.NONE){
					//Use none of the filter of the linguistic resource, so remove all the filter and 
					//then exit the for
					searchFilterString = new ArrayList<String>();
					break;
				}
				else{
					searchFilterString.add(searchFilter.getName());
				}
			}
		}
		
		
		try {
			if(searchFilterString.contains("Exact search")){
				if (searchFilterString.contains("Polysemy_NOUN"))
					indexWord = Dictionary.getInstance().getIndexWord(POS.NOUN, word);
				if(indexWord == null && searchFilterString.contains("Polysemy_VERB")){
					indexWord = Dictionary.getInstance().getIndexWord(POS.VERB, word);
				}
				if(indexWord == null && searchFilterString.contains("Polysemy_ADJECTIVE")){
					indexWord = Dictionary.getInstance().getIndexWord(POS.ADJECTIVE, word);
				}
				if(indexWord == null && searchFilterString.contains("Polysemy_ADVERB")){
					indexWord = Dictionary.getInstance().getIndexWord(POS.ADVERB, word);
				}
				
				if(indexWord != null) {
					searchedWord.add(new SearchWord(indexWord.getLemma()));
				}
			}
			else{
				//Use the MorphologicalProcessor to search for all the possible lemma of the word
				MorphologicalProcessor morph = Dictionary.getInstance().getMorphologicalProcessor();
				List<String> stringList = new ArrayList<String>();
				
				if(searchFilterString.contains("Polysemy_NOUN"))  
					stringList = mergeNoDuplcate(stringList, morph.lookupAllBaseForms(POS.NOUN, word));
				if(searchFilterString.contains("Polysemy_VERB")) 
					stringList = mergeNoDuplcate(stringList, morph.lookupAllBaseForms(POS.VERB, word));
				if(searchFilterString.contains("Polysemy_ADJECTIVE")) 
					stringList = mergeNoDuplcate(stringList, morph.lookupAllBaseForms(POS.ADJECTIVE, word));
				if(searchFilterString.contains("Polysemy_ADVERB")) 
					stringList = mergeNoDuplcate(stringList, morph.lookupAllBaseForms(POS.ADVERB, word));
				for(String lemma : stringList){
					searchedWord.add(new SearchWord(lemma));
				}
			}
		}
		catch (JWNLException ex) {ex.printStackTrace();}
		
		
		return searchedWord;
	}
	
	private ArrayList<String> mergeNoDuplcate(List<String> listA, List<String> listB) {
		ArrayList<String> newList = new ArrayList<String>();
		for(String element : listA) {
			if(!newList.contains(element)){
				newList.add(element);
			}
		}
		for(String element : listB) {
			if(!newList.contains(element)){
				newList.add(element);
			}
		}
		
		return newList;
	}


	@Override
	public Collection<SemanticIndex> exploreLexicalRelation(String wordString, SemanticIndex c, LexicalRelation relation) throws LinguisticResourceAccessException {
		ArrayList <SemanticIndex>result = new ArrayList<SemanticIndex>();
		PointerTargetNodeList synsetsWordsList;
		Word word = null;
		for(Word tempWord: ((WordnetConcept)c).getSynset().getWords()){
        	if(tempWord.getLemma().compareToIgnoreCase(wordString) == 0){
        		word = tempWord;
        		break;
        	}
        }
		
		try{
			if(JMWNL.isEWN())
				synsetsWordsList = new PointerTargetNodeList(((WordnetConcept)c).getSynset().getTargets(relationPointerTypeMap.get(relation.getName())));
			else
				synsetsWordsList = new PointerTargetNodeList(word.getTargets(relationPointerTypeMap.get(relation.getName())));
			java.util.ListIterator sensesIterator = synsetsWordsList.listIterator();
	        synsetsWordsList.print();
	        System.out.println(synsetsWordsList.size());
	        while (sensesIterator.hasNext()){
	        	PointerTargetNode pointerTargetNode = (PointerTargetNode)sensesIterator.next();
	            if(JMWNL.isEWN())
	            	result.add(	new WordnetConcept(pointerTargetNode.getSynset()));
	            else
	            	result.add(	new WordInWordnetConcept(pointerTargetNode.getSynset(), pointerTargetNode.getWord().getLemma()	)	);
	        }
		}
		catch (JWNLException e) {
			throw new LinguisticResourceAccessException(e);
		}
        
        return result;
	}


	@Override
	public Collection<SemanticIndex> exploreSemanticRelation(SemanticIndex c, SemanticRelation relation) throws LinguisticResourceAccessException {
		ArrayList <SemanticIndex>result = new ArrayList<SemanticIndex>();
		Synset synset = ((WordnetConcept)c).getSynset();
        PointerTargetNodeList synsetsList;
        try{
        	synsetsList = new PointerTargetNodeList(synset.getTargets(relationPointerTypeMap.get(relation.getName())));
	        
	        ListIterator <PointerTargetNode>sensesIterator = synsetsList.listIterator();
	        synsetsList.print();
	        System.out.println(synsetsList.size());
	        
	        while (sensesIterator.hasNext())
	            result.add(	new WordnetConcept((sensesIterator.next()).getSynset()) );
		}
        catch (JWNLException e) {
			throw new LinguisticResourceAccessException(e);
		}
        return result;     
	}
    
    
    
    
    
}
