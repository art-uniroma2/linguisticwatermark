package it.uniroma2.art.lw.wordnet;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

public class SAXHandlerProperties extends DefaultHandler {
	public String language;
	public String version;
	public SAXHandlerProperties() {
		language = null;
		version = null;
	}	
					
	public void startDocument() {
	}
	
	public void endDocument() {
	}
	
	public void startElement(String uri, String localName, String qname,
							 Attributes attr) {
		if (qname.equals("version")) {
			for (int i = 0; i < attr.getLength(); i++) {
				if (i == 1)
					version = new String(attr.getValue(i));			    
				if (i == 2)					
					language = new String(attr.getValue(i));
			}
		}
	}
}