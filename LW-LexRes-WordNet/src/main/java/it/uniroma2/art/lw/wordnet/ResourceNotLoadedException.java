package it.uniroma2.art.lw.wordnet;

public class ResourceNotLoadedException extends Exception {
	String errorMessage;
	
	public ResourceNotLoadedException() {
		super();
	}
	
	public ResourceNotLoadedException(String s) {
		super(s);
		errorMessage = s;
	}
	public String toString() {
		return errorMessage;
	}	
}

