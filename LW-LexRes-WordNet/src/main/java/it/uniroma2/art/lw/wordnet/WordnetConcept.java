 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http://www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is WordNetInterface.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2005.
  * All Rights Reserved.
  *
  * Linguistic Watermark was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata (ART)
  * Current information about Linguistic Watermark can be obtained at 
  * http://art.uniroma2.it/software/lw/wordnetinterface
  *
  */


/*
 * ConceptHandlingFacility.java
 *
 * Created on 8 aprile 2004, 18.15
 */

package it.uniroma2.art.lw.wordnet;

import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.dictionary.Dictionary;

/**
 *
 * @author  Armando Stellato
 */
public class WordnetConcept extends SemanticIndex {

    public POS pos;
    public Synset synset;
    
    protected WordnetConcept(String POSSynset) throws SemIndexRetrievalException {
        String[] data = POSSynset.split("\\.");
        pos = POS.getPOSForLabel(data[0]);
        try { synset = Dictionary.getInstance().getSynsetAt(pos, Long.parseLong(data[1])); }
        catch (Exception e) {throw new SemIndexRetrievalException();}
    }
    
    protected WordnetConcept(Synset synset) {
	this.synset = synset;
        pos = synset.getPOS();
    }        
    
    public POS getPOS() {
        return pos;
    }

    public Synset getSynset() {
        return synset;
    }
    
    public String getPOSSynsetID() {
        return pos.getLabel() + "." + synset.getOffset();
    }
    
    public static String getPOSSynsetID(Synset synset) {
        return synset.getPOS().getLabel() + "." + synset.getOffset();
    }

    /* (non-Javadoc)
     * @see it.uniroma2.info.ai_nlp.LinguisticWatermark.Concept#getConceptRepresentation()
     * uguale a getPOSSynsetID che va infatti sostituito in tutte le chiamate
     */
    public String getConceptRepresentation() {
        return synset.getPOS().getLabel() + "." + synset.getOffset();
    }
    
    public boolean equals(SemanticIndex c) {
        return getSynset().equals( ((WordnetConcept)c).getSynset() );
    }
    
    public String toString() {
        return getConceptRepresentation();
    }
}
