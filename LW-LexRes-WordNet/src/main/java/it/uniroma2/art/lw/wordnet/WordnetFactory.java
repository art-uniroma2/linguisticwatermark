package it.uniroma2.art.lw.wordnet;

import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;

public class WordnetFactory extends LIFactory {


    public WordnetFactory(String id) {
        super(id, WordnetInterface.class);
    }

    @Override
    public LinguisticInterface getLinguisticInterface() {
        WordnetInterface wordnetInterface = new WordnetInterface();        
        return wordnetInterface;
    }

}
