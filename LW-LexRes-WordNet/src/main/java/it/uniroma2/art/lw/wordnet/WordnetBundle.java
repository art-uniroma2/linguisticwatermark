package it.uniroma2.art.lw.wordnet;

import it.uniroma2.art.lw.model.objects.LIFactory;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class WordnetBundle implements BundleActivator{

	public void start(BundleContext context) throws Exception {
		System.out.println("WORDNETFACTORY ADDED to FELIX");
		context.registerService(LIFactory.class.getName(), 
				new WordnetFactory("WORDNET"), null);
		
	}

	public void stop(BundleContext arg0) throws Exception {
		
		
	}

}
